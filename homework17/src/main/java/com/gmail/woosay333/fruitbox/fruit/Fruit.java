package com.gmail.woosay333.fruitbox.fruit;


import lombok.Data;

@Data
public abstract class Fruit {

    private final String name;
    private final float weight;

}
