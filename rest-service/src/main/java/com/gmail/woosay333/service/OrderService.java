package com.gmail.woosay333.service;

import com.gmail.woosay333.entity.Order;
import com.gmail.woosay333.repository.OrderRepository;

import java.util.Collection;
import java.util.UUID;

public class OrderService {

    private final OrderRepository repository = new OrderRepository();

    public Order addOrder(Order order) {
        return repository.addOrder(order);
    }

    public Order getOrder(UUID id) {
        return repository.getOrder(id);
    }

    public Collection<Order> getOrders() {
        return repository.getOrders();
    }

}
