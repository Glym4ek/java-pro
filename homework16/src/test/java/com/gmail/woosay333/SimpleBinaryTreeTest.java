package com.gmail.woosay333;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SimpleBinaryTreeTest {

    private SimpleBinaryTree<Integer> tree;

    @BeforeEach
    void setUp() {
        tree = new SimpleBinaryTree<>();
    }

    @Test
    void rootNodeShouldBeNullIfTreeIsEmpty() {
        assertNull(tree.getRoot());
    }

    @ParameterizedTest
    @MethodSource("treeAddArguments")
    void addMethodTest(List<Integer> arguments) {
        for (Integer value : arguments) {
            tree.add(value);
        }

        assertAll(() -> assertEquals(8, tree.getRoot().getValue()),
                () -> assertEquals(3, tree.getRoot().getLeft().getValue()),
                () -> assertEquals(1, tree.getRoot().getLeft().getLeft().getValue()),
                () -> assertEquals(6, tree.getRoot().getLeft().getRight().getValue()),
                () -> assertEquals(4, tree.getRoot().getLeft().getRight().getLeft().getValue()),
                () -> assertEquals(7, tree.getRoot().getLeft().getRight().getRight().getValue()),
                () -> assertEquals(10, tree.getRoot().getRight().getValue()),
                () -> assertEquals(14, tree.getRoot().getRight().getRight().getValue()),
                () -> assertEquals(13, tree.getRoot().getRight().getRight().getLeft().getValue()));

    }

    @ParameterizedTest
    @MethodSource("treeAddArguments")
    void traverseThrowSimpleTreeMethodShouldPrintAllTreeNodesInOrder(List<Integer> arguments) {
        for (Integer value : arguments) {
            tree.add(value);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream outTemp = System.out;
        System.setOut(new PrintStream(out));

        tree.traverseThrowSimpleTree();

        assertEquals("1 3 4 6 7 8 10 13 14 ", out.toString());

        System.setOut(outTemp);
    }

    private static Stream<Arguments> treeAddArguments() {
        return Stream.of(Arguments.of(List.of(8, 3, 6, 10, 14, 1, 4, 7, 13)));
    }


    @AfterEach
    void tearDown() {
        tree = null;
    }

}
