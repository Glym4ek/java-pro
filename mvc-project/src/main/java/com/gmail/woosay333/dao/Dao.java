package com.gmail.woosay333.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface Dao<T> {

    List<T> getAll() throws SQLException;

    int add(T entity) throws SQLException;

    void remove(int id) throws SQLException;

    Optional<T> get(int id) throws SQLException;

}
