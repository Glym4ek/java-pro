package com.gmail.woosay333;

import static java.lang.System.out;

public class GitTest {
    public static void main(String[] args) {
        out.println("Hello Git from main branch!");
        out.println("Hello GitLab from main branch!");
        out.println("Hello GitLab from homework2 branch!");
    }
}
