package com.gmail.woosay333;

import static java.lang.System.out;

public class Dog extends Animal {

    private static int counterOfCreatedClasses = 0;
    private int runDistance;
    private int swimDistance;

    public Dog(String name) {
        super(name);
        runDistance = 500;
        swimDistance = 10;
        ++counterOfCreatedClasses;
    }

    public static int getCounterOfCreatedClasses() {
        return counterOfCreatedClasses;
    }

    @Override
    public void run(int obstacleLength) {
        if (runDistance == 0) {
            out.println("Dog " + getName() + " cannot run any more...");
        } else if (runDistance - obstacleLength >= 0) {
            runDistance -= obstacleLength;
            out.println("Dog " + getName() + " ran " + obstacleLength + " m.");
        } else {
            out.println("The length of the obstacle is too long...");
        }
    }

    @Override
    public void swim(int obstacleLength) {
        if (swimDistance == 0) {
            out.println("Dog " + getName() + " cannot swim any more...");
        } else if (swimDistance - obstacleLength >= 0) {
            swimDistance -= obstacleLength;
            out.println("Dog " + getName() + " swam " + obstacleLength + " m.");
        } else {
            out.println("The length of the obstacle is too long...");
        }
    }

    @Override
    public String toString() {
        return "Dog {" +
                "name: " + getName() +
                ", available to run: " + runDistance + " m." +
                ", available to swim: " + swimDistance + " m." +
                '}';
    }

}
