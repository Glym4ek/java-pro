package com.gmail.woosay333.exception;

public class NumberNotExistsException extends RuntimeException {

    public NumberNotExistsException(String message) {
        super(message);
    }

}
