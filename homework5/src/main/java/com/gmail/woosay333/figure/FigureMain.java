package com.gmail.woosay333.figure;

import static java.lang.System.out;

public class FigureMain {

    public static void main(String[] args) {
        Figure circle = new Circle(2);
        out.println(circle.getArea());

        Figure triangle = new Triangle(7, 10, 5);
        out.println(triangle.getArea());

        Figure square = new Square(4);
        out.println(square.getArea());

        Figure[] figures = {circle, triangle, square};
        out.println(Util.getTotalArea(figures));
    }

}
