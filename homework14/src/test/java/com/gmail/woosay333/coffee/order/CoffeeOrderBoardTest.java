package com.gmail.woosay333.coffee.order;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CoffeeOrderBoardTest {

    private CoffeeOrderBoard board;

    @BeforeEach
    void setUp() {
        board = new CoffeeOrderBoard();
    }

    @ParameterizedTest
    @NullSource
    void addShouldThrowIllegalArgumentExceptionWhenInputStringIsNull(String input) {
        assertFalse(board.add(input));
    }

    @ParameterizedTest
    @EmptySource
    void addShouldThrowIllegalArgumentExceptionWhenInputStringIsEmpty(String input) {
        assertFalse(board.add(input));
    }

    @ParameterizedTest
    @MethodSource("blankStringArguments")
    void addShouldThrowIllegalArgumentExceptionWhenInputStringIsBlank(String input) {
        assertFalse(board.add(input));
    }

    @ParameterizedTest
    @MethodSource("stringNamesArguments")
    void addShouldReturnTrueIfElementWasAdded(String name) {
        assertTrue(board.add(name));
    }

    @Test
    void deliverShouldPrintMessageIfQueueIsEmpty() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        board.deliver();

        assertEquals(String.format("Queue is empty!%n"), out.toString());
    }

    @Test
    void deliverShouldPrintMessageWithOrderAndDeleteItFromQueue() {
        board.add("Ron");
        board.add("Garry");
        board.add("Marry");

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        board.deliver();

        assertTrue(out.toString().contains("Issuing an order for no 1 for client Ron"));
    }

    @Test
    void deliverShouldThrowIllegalArgumentExceptionWhenIdIsLessThenOne() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        board.deliver(0);

        assertTrue(out.toString().contains("Order id is less than one"));
    }

    @Test
    void deliverShouldThrowIllegalArgumentExceptionWhenIdIsLessThenZero() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        board.deliver(-1);

        assertTrue(out.toString().contains("Order id is less than one"));
    }

    @Test
    void deliverShouldPrintMessageWhenIdNotFound() {
        board.add("Ron");
        board.add("Garry");
        board.add("Marry");

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        board.deliver(4);

        assertTrue(out.toString().contains("Order with id = 4 not found"));
    }

    @Test
    void deliverShouldPrintMessageIfIdWasFound() {
        board.add("Ron");
        board.add("Garry");
        board.add("Marry");

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        board.deliver(3);

        assertTrue(out.toString().contains("Issuing an order for no 3 for client Marry"));
    }

    @Test
    void drawShouldPrintMessageIfQueueEmpty() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        board.draw();

        assertTrue(out.toString().contains("Queue is empty"));
    }

    @Test
    void drawShouldPrintNumbersAndNamesOfAllOrders() {
        board.add("Ron");
        board.add("Garry");
        board.add("Ketty");
        board.add("Marry");
        board.add("Shon");

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        board.draw();

        assertAll(() -> assertTrue(out.toString().contains("1 | Ron")),
                () -> assertTrue(out.toString().contains("2 | Garry")),
                () -> assertTrue(out.toString().contains("3 | Ketty")),
                () -> assertTrue(out.toString().contains("4 | Marry")),
                () -> assertTrue(out.toString().contains("5 | Shon"))
        );
    }

    @AfterEach
    void teardown() {
        board = null;
    }

    private static Stream<String> blankStringArguments() {
        return Stream.of(" ", "  ", "   ", "    ", "     ");
    }

    private static Stream<String> stringNamesArguments() {
        return Stream.of("Ron", "Garry", "Ketty", "Garry", "Ron", "Marry", "Elizabet", "Shon", "Barbara");
    }

}
