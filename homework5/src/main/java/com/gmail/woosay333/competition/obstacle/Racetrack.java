package com.gmail.woosay333.competition.obstacle;

import com.gmail.woosay333.competition.member.Member;

import static java.lang.System.out;

public class Racetrack extends Obstacle {

    private final double length;

    public Racetrack(String name, double length) {
        super(name);
        if (length <= 0) {
            throw new IllegalArgumentException("Invalid value of racetrack length...");
        }
        this.length = length;
    }

    @Override
    public boolean overcome(Member member) {
        member.run();

        if (member.getRunLimit() >= length) {
            out.println("Member " + member.getName() + " overcame " + getName() + " " + length + " m.");
            member.addPassedDistance(length);
            member.setCountOfPassedObstacles();
            return true;
        } else {
            out.println("Member " + member.getName() + " didn't overcome " + getName() + " " + length + " m.");
            return false;
        }
    }

}
