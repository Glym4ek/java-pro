package com.gmail.woosay333;

import com.gmail.woosay333.config.ApplicationConfig;
import com.gmail.woosay333.service.ConsoleShoppingCartApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ApplicationRunner {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(ApplicationConfig.class);
        context.refresh();

        ConsoleShoppingCartApplication application = context.getBean(ConsoleShoppingCartApplication.class);
        application.start();

        context.close();
    }

}
