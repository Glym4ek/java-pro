package com.gmail.woosay333.util;

import com.gmail.woosay333.model.Product;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;

import java.io.Reader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProductsReaderFromJSON {

    private ProductsReaderFromJSON() {
    }

    public static Map<Integer, Product> getEntityList(String path) {
        Map<Integer, Product> entities = new HashMap<>();

        try (Reader reader = new FileReader(path)) {
            Gson gson = new Gson();
            JsonArray jsonArray = JsonParser.parseReader(reader).getAsJsonArray();

            for (JsonElement element : jsonArray) {
                Product product = gson.fromJson(element, Product.class);
                entities.put(product.getId(), product);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return entities;
    }

}
