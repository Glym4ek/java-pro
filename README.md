# Course expectations:
- ### learn the Java language;
- ### learn to work with OOP;
- ### gain knowledge and skills to create Java applications;
- ### learn to apply a set of technologies included in the standard JEE stack and a range of related technologies;