package com.gmail.woosay333.competition.obstacle;

import com.gmail.woosay333.competition.member.Member;

public abstract class Obstacle {

    private final String name;

    protected Obstacle(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract boolean overcome(Member member);

}
