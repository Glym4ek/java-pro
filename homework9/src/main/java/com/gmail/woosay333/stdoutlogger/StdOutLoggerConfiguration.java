package com.gmail.woosay333.stdoutlogger;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.loggerfactory.LoggerConfiguration;

public class StdOutLoggerConfiguration implements LoggerConfiguration {

    private final LoggerLevel level;

    public StdOutLoggerConfiguration(LoggerLevel level) {
        this.level = level;
    }

    @Override
    public LoggerLevel getLoggerLevel() {
        return level;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() +
                " {" +
                "level=" + level +
                '}';
    }

}
