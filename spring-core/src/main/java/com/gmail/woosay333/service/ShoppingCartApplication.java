package com.gmail.woosay333.service;

import com.gmail.woosay333.model.Cart;
import com.gmail.woosay333.model.Product;
import com.gmail.woosay333.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static java.lang.System.out;

@Slf4j
public class ShoppingCartApplication {

    private final ProductRepository productRepository;
    private final Cart cart;

    public ShoppingCartApplication(ProductRepository productRepository, Cart cart) {
        this.productRepository = productRepository;
        this.cart = cart;
    }

    public void addToCart(int id) {
        Product product = productRepository.getProductById(id);

        if (product != null) {
            cart.addProduct(product);
            log.info("Product added to cart: " + product.getName());
        } else {
            log.error("Product not found.");
        }
    }

    public void removeFromCart(int id) {
        Product product = productRepository.getProductById(id);

        if (product != null) {
            cart.removeProduct(id);
            log.info("Product removed from cart: " + product.getName());
        } else {
            log.error("Product not found in cart.");
        }
    }

    public void printCartInfo() {
        List<Product> products = cart.getProducts();

        if (products.isEmpty()) {
            log.warn("Cart is empty.");
        } else {
            log.info("Cart contents:");
            products.forEach(out::println);
        }
    }

}
