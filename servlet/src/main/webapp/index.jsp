<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Hello Servlet</title>
    </head>
    <body style="text-align: center">
        <h2>Hello Servlet!</h2>

        <form name="messageForm" method="post" action="messageServlet">
            Username: <input type="text" name="username"/> <br/>
            <input type="submit" value="Send" />
        </form>
    </body>
</html>
