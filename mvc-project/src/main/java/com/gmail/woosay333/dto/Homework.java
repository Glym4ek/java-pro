package com.gmail.woosay333.dto;

import lombok.Data;
import lombok.Builder;

@Data
@Builder
public class Homework {

    private int id;
    private String name;
    private String description;

}
