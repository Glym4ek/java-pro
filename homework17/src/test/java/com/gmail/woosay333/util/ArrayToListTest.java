package com.gmail.woosay333.util;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ArrayToListTest {

    @Test
    void arrayToListMethodShouldReturnListFromInputArray() {
        Integer[] inputIntegers = {1, 2, 3, 4, 5, 6, 7};
        List<Integer> expectedIntegers = List.of(inputIntegers);

        String[] inputStrings = {"test1", "test2", "test3"};
        List<String> expectedStrings = List.of(inputStrings);

        assertAll(() -> assertInstanceOf(ArrayList.class, ArrayToList.arrayToList(inputIntegers)),
                () -> assertInstanceOf(ArrayList.class, ArrayToList.arrayToList(inputStrings)),
                () -> assertEquals(expectedIntegers, ArrayToList.arrayToList(inputIntegers)),
                () -> assertEquals(expectedStrings, ArrayToList.arrayToList(inputStrings)));
    }

}
