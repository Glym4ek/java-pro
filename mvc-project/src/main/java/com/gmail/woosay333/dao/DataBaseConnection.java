package com.gmail.woosay333.dao;

import com.gmail.woosay333.util.PropertyReader;
import com.mysql.cj.jdbc.MysqlDataSource;
import lombok.SneakyThrows;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public enum DataBaseConnection {

    INSTANCE;

    private final DataSource dataSource;

    DataBaseConnection() {
        this.dataSource = getDataSource();
    }

    private DataSource getDataSource() {
        MysqlDataSource mysqlDataSource = new MysqlDataSource();
        Properties properties = PropertyReader.getProperties("application.properties");

        mysqlDataSource.setUrl(properties.getProperty("db.url"));
        mysqlDataSource.setUser(properties.getProperty("db.user"));
        mysqlDataSource.setPassword(properties.getProperty("db.password"));

        return mysqlDataSource;
    }

    @SneakyThrows
    public Connection getConnection() {
        return dataSource.getConnection();
    }

    public void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
