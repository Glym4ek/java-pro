package com.gmail.woosay333.figure;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Circle implements Figure {

    private final double radius;

    public Circle(double radius) {
        if (radius > 0) {
            this.radius = radius;
        } else {
            throw new IllegalArgumentException("Circle is not valid...");
        }
    }

    @Override
    public double getArea() {
        return PI * pow(radius, 2);
    }

    @Override
    public String toString() {
        return "Circle {" +
                "radius = " + radius +
                '}';
    }

}
