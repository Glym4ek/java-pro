package com.gmail.woosay333.repository;

import com.gmail.woosay333.entity.Order;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class OrderRepository {

    private final Map<UUID, Order> storage = new HashMap<>();

    public Order addOrder(Order order) {
        storage.put(order.getId(), order);
        return order;
    }

    public Order getOrder(UUID id) {
        return storage.get(id);
    }

    public Collection<Order> getOrders() {
        return storage.values();
    }

}
