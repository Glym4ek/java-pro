package com.gmail.woosay333.factory;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FurnitureFactoryTest {

    @Test
    void getFurnitureMethodShouldReturnInstanceOfClassWhichIsPassedAsArgument() {
        Furniture chair = FurnitureFactory.getFurniture(Chair.class, "Chair", BigDecimal.valueOf(970.5));
        Furniture sofa = FurnitureFactory.getFurniture(Sofa.class, "Sofa", BigDecimal.valueOf(11546.3));
        Furniture closet = FurnitureFactory.getFurniture(Closet.class, "Closet", BigDecimal.valueOf(9870.0));

        assertAll(() -> assertInstanceOf(Chair.class, chair),
                () -> assertEquals("Chair {name = 'Chair', price = 970.5}", chair.toString()),
                () -> assertInstanceOf(Sofa.class, sofa),
                () -> assertEquals("Sofa {name = 'Sofa', price = 11546.3}", sofa.toString()),
                () -> assertInstanceOf(Closet.class, closet),
                () -> assertEquals("Closet {name = 'Closet', price = 9870.0}", closet.toString()));
    }

}
