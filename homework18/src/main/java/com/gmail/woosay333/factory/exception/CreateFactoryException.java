package com.gmail.woosay333.factory.exception;

public class CreateFactoryException extends RuntimeException {

    public CreateFactoryException(String message) {
        super(message);
    }

}
