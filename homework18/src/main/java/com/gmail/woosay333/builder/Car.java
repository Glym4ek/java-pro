package com.gmail.woosay333.builder;

import lombok.Value;

@Value
public class Car {

    String engine;
    String transmission;
    int seats;
    String navigator;
    String color;

}
