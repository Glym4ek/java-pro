DROP DATABASE IF EXISTS homework32;

CREATE DATABASE IF NOT EXISTS homework32;

CREATE USER IF NOT EXISTS 'admin_homework32'@'localhost' IDENTIFIED BY '0000';

GRANT ALL PRIVILEGES ON homework32.* TO 'admin_homework32'@'localhost';

USE homework32;


CREATE TABLE product
(
    id    INT PRIMARY KEY AUTO_INCREMENT,
    name  VARCHAR(255)  NOT NULL,
    price DECIMAL(8, 2) NOT NULL
);


CREATE TABLE cart
(
    id INT PRIMARY KEY AUTO_INCREMENT
);


CREATE TABLE cart_product
(
    cart_id    INT NOT NULL,
    product_id INT NOT NULL,
    FOREIGN KEY (cart_id) REFERENCES cart (id),
    FOREIGN KEY (product_id) REFERENCES product (id)
);