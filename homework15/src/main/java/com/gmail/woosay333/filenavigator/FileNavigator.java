package com.gmail.woosay333.filenavigator;

import com.gmail.woosay333.dto.FileData;
import lombok.Data;
import lombok.NonNull;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.Comparator;
import java.util.stream.Collectors;

@Data
public class FileNavigator {

    private final Map<String, Set<FileData>> container;

    public FileNavigator() {
        this.container = new HashMap<>();
    }

    public void add(@NonNull String path, @NonNull FileData file) {
        if (!path.equals(file.getPath())) {
            throw new IllegalArgumentException(String.format("Can`t add file with path %s to the directory with path %s!", file.getPath(), path));
        }

        container.computeIfAbsent(path, s -> new HashSet<>()).add(file);
    }

    public Set<FileData> find(@NonNull String path) {
        return container.getOrDefault(path, new HashSet<>());
    }

    public Set<FileData> filterBySize(@NonNull String path, @NonNull long size) {
        return find(path).stream().filter(fileData -> fileData.getSize() <= size).collect(Collectors.toSet());
    }

    public void remove(String path) {
        container.remove(path);
    }

    public Set<FileData> sortBySize(String path) {
        Set<FileData> sortedSet = new TreeSet<>(Comparator.comparingLong(FileData::getSize));
        sortedSet.addAll(find(path));
        return sortedSet;
    }

}
