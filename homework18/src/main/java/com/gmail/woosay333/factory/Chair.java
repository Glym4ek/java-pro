package com.gmail.woosay333.factory;

import java.math.BigDecimal;

public class Chair extends Furniture {

    public Chair(String name, BigDecimal price) {
        super(name, price);
    }

}
