package com.gmail.woosay333.dao;

import com.gmail.woosay333.dto.Homework;
import com.gmail.woosay333.dto.Lesson;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static java.lang.System.out;

class LessonDaoTest {

    @Test
    @SneakyThrows
    void getAllTest() {
        new LessonDao().getAll().forEach(out::println);
    }

    @Test
    @SneakyThrows
    void addTest() {
        int id = new LessonDao().add(Lesson.builder()
                .name("Test add method")
                .homework(Homework.builder()
                        .id(5)
                        .build())
                .build());

        out.println(id);
    }

    @Test
    @SneakyThrows
    void removeTest() {
        new LessonDao().remove(10);
    }

    @Test
    @SneakyThrows
    void getTest() {
        new LessonDao().get(5).ifPresent(out::println);
    }

}
