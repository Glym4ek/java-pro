package com.gmail.woosay333.coffee.order;

import lombok.Value;

@Value
public class Order {

    long orderNumber;
    String clientName;

}
