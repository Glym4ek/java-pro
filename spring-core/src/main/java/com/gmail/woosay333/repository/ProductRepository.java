package com.gmail.woosay333.repository;

import com.gmail.woosay333.model.Product;
import com.gmail.woosay333.util.ProductsReaderFromJSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProductRepository {

    private final Map<Integer, Product> products;

    public ProductRepository(String path) {
        this.products = ProductsReaderFromJSON.getEntityList(path);
    }

    public List<Product> getAllProducts() {
        return new ArrayList<>(products.values());
    }

    public Product getProductById(int id) {
        return products.get(id);
    }

}
