package com.gmail.woosay333.petrolstation;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

class PetrolStationTest {

    @SneakyThrows
    @Test
    void petrolStationTest() {
        PetrolStation petrolStation = new PetrolStation(440.5, 3);
        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        Stream.<Runnable>of(
                        () -> petrolStation.doRefuel(70.2),
                        () -> petrolStation.doRefuel(35.8),
                        () -> petrolStation.doRefuel(20),
                        () -> petrolStation.doRefuel(85),
                        () -> petrolStation.doRefuel(90),
                        () -> petrolStation.doRefuel(45),
                        () -> petrolStation.doRefuel(46.5),
                        () -> petrolStation.doRefuel(30),
                        () -> petrolStation.doRefuel(25)
                )
                .forEach(threadPool::submit);

        threadPool.shutdown();
        threadPool.awaitTermination(5L, TimeUnit.MINUTES);
    }

}
