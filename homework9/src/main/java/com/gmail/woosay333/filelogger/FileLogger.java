package com.gmail.woosay333.filelogger;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.exception.CreateNewFileException;
import com.gmail.woosay333.exception.FileMaxSizeReachedException;
import com.gmail.woosay333.exception.MyInterruptException;
import com.gmail.woosay333.loggerfactory.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileSystems;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileLogger implements Logger {

    private final FileLoggerConfiguration configuration;
    private File logFile;

    public FileLogger(FileLoggerConfiguration configuration) {
        this.configuration = configuration;
        this.logFile = createNewLogFile();
    }

    public File getLogFile() {
        return logFile;
    }

    @Override
    public void debug(String message) {
        log(LoggerLevel.DEBUG, message);
    }

    @Override
    public void info(String message) {
        log(LoggerLevel.INFO, message);
    }

    private void log(LoggerLevel level, String message) {
        String formattedMessage = getFormattedMessage(level, message);

        try {
            checkFileSize(message);
        } catch (FileMaxSizeReachedException ex) {
            logFile = createNewLogFile();
        }

        if (level.ordinal() <= configuration.getLoggerLevel().ordinal()) {
            writeLogToFile(formattedMessage);
        }
    }

    private void writeLogToFile(String message) {
        try (Writer writer = new FileWriter(logFile, true)) {
            writer.write(message);
            writer.write(System.lineSeparator());
        } catch (IOException ex) {
            throw new CreateNewFileException("Can`t write message " + message + " into file " + logFile.getAbsolutePath() + "!");
        }
    }

    private void checkFileSize(String message) throws FileMaxSizeReachedException {
        long currentFileSize = logFile.length();
        long messageSize = message.getBytes().length;

        if (currentFileSize + messageSize > configuration.getFileMaxSize()) {
            String errMessage = "Maximum file size exceeded! Max file size: " +
                    configuration.getFileMaxSize() +
                    ". Current file size: " +
                    logFile.length() +
                    ". File path: " +
                    logFile.getAbsolutePath();
            throw new FileMaxSizeReachedException(errMessage);
        }
    }

    private File createNewLogFile() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd_MM_yyyy-HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        String newFilePath = configuration.getLogFilePath() +
                FileSystems.getDefault().getSeparator() +
                dtf.format(now) +
                "." +
                configuration.getFileFormat();
        File newFile = new File(newFilePath);

        try {
            newFile.createNewFile();
        } catch (IOException ex) {
            throw new CreateNewFileException("Can`t create new file with path " + newFilePath + "!");
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            throw new MyInterruptException("Thread is interrupted!");
        }

        return newFile;
    }

}
