package com.gmail.woosay333.dao;

import com.gmail.woosay333.config.HibernateSession;
import com.gmail.woosay333.entity.Student;
import jakarta.persistence.Table;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class StudentDAOTest {

    private static DAO<Student> studentDAO;

    @BeforeAll
    static void init() {
        studentDAO = new StudentDAO();
    }

    @Order(1)
    @Test
    void insertTest() {
        Student student1 = Student.builder()
                .name("Student1")
                .email("student1@email.com")
                .build();
        Student student2 = Student.builder()
                .name("Student2")
                .email("student2@email.com")
                .build();
        Student student3 = Student.builder()
                .name("Student3")
                .email("student3@email.com")
                .build();
        Student student4 = Student.builder()
                .name("Student4")
                .email("student4@email.com")
                .build();
        Student student5 = Student.builder()
                .name("Student5")
                .email("student5@email.com")
                .build();

        studentDAO.insert(student1);
        studentDAO.insert(student2);
        studentDAO.insert(student3);
        studentDAO.insert(student4);
        studentDAO.insert(student5);
    }

    @Order(2)
    @Test
    void updateTest() {
        Student student6 = Student.builder()
                .name("Student6")
                .email("student6@email.com")
                .build();

        studentDAO.insert(student6);

        student6.setName("Student6 updated name");
        student6.setEmail("Student6@email.updated");

        studentDAO.update(student6);
    }

    @Order(3)
    @Test
    void deleteTest() {
        studentDAO.delete(1);
        assertNull(studentDAO.getById(1));
    }

    @Order(4)
    @Test
    void getByIdTest() {
        assertEquals("Student2", studentDAO.getById(2).getName());
    }

    @Order(5)
    @Test
    void getAllTest() {
        Collection<Student> studentList = studentDAO.getAll();
        assertFalse(studentList.isEmpty());
    }

    @AfterAll
    static void destroy() {
        try (Session session = HibernateSession.INSTANCE.openSession()) {
            Transaction transaction = session.beginTransaction();
            String truncateQuery =
                    String.format("TRUNCATE TABLE %s", Student.class.getAnnotation(Table.class).name());
            session.createNativeQuery(truncateQuery).executeUpdate();
            transaction.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
