package com.gmail.woosay333.figure;

import java.util.Arrays;

public class Util {

    private Util() {
    }

    static double getTotalArea(Figure[] array) {
        return Arrays.stream(array).mapToDouble(Figure::getArea).sum();
    }

}
