package com.gmail.woosay333;

import com.gmail.woosay333.exceptions.ArrayDataException;
import com.gmail.woosay333.exceptions.ArraySizeException;

public class ArrayValueCalculator {

    private ArrayValueCalculator() {
    }

    public static int doCalc(String[][] array) throws ArraySizeException, ArrayDataException {
        if (!isArrayValid(array)) {
            throw new ArraySizeException("Sorry, wrong array size!");
        }

        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                try {
                    sum += Integer.parseInt(array[i][j]);
                } catch (NumberFormatException ex) {
                    throw new ArrayDataException("Can't convert string to number in column [" + i + "] in row [" + j + "]!");
                }
            }
        }

        return sum;
    }

    private static boolean isArrayValid(String[][] array) {
        for (String[] element : array) {
            if (array.length != element.length) {
                return false;
            }
        }
        return true;
    }

}
