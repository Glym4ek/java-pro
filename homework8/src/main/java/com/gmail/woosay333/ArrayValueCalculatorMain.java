package com.gmail.woosay333;

import com.gmail.woosay333.exceptions.ArrayDataException;
import com.gmail.woosay333.exceptions.ArraySizeException;

import java.util.Arrays;

import static java.lang.System.out;

public class ArrayValueCalculatorMain {

    public static void main(String[] args) {
        String[][] validMatrix = {
                {"1", "2", "3", "4"},
                {"5", "6", "7", "8"},
                {"9", "10", "11", "12"},
                {"13", "14", "15", "16"}
        };

        try {
            out.printf("Sum of given array %s elements is: %d%n", Arrays.deepToString(validMatrix), ArrayValueCalculator.doCalc(validMatrix));
        } catch (NullPointerException ex) {
            out.println("Given array is null!");
        } catch (ArraySizeException | ArrayDataException ex) {
            out.println(ex.getMessage());
        }
    }

}
