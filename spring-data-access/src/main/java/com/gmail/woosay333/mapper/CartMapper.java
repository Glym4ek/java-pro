package com.gmail.woosay333.mapper;

import com.gmail.woosay333.dto.Cart;
import com.gmail.woosay333.dto.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CartMapper implements RowMapper<Cart> {

    private final ProductMapper productMapper;

    public CartMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Override
    public Cart mapRow(ResultSet rs, int rowNum) throws SQLException {
        Cart cart = Cart.builder()
                .id(rs.getInt("id"))
                .build();

        List<Product> products = new ArrayList<>();

        do {
            products.add(productMapper.mapRow(rs, rowNum));
        } while (rs.next() && cart.getId() == rs.getInt("id"));

        cart.setProducts(products);

        return cart;
    }

}
