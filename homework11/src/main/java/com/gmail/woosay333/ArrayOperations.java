package com.gmail.woosay333;

import com.gmail.woosay333.exception.NumberNotExistsException;

import java.util.Arrays;

public class ArrayOperations {

    private ArrayOperations() {
    }

    public static int[] getArrayTruncatedAfterLastGivenNumber(int[] inputArray, int number) {
        if (inputArray == null || inputArray.length == 0) {
            throw new IllegalArgumentException("Sorry, input array is null or empty!");
        }

        int index = -1;

        for (int i = inputArray.length - 1; i >= 0; i--) {
            if (inputArray[i] == number) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            throw new NumberNotExistsException("Input array does`t exists number " + number + "!");
        }

        return Arrays.copyOfRange(inputArray, index + 1, inputArray.length);
    }

    public static boolean checkArrayNumbersExists(int[] inputArray, int firstNumber, int secondNumber) {
        if (inputArray == null || inputArray.length == 0) {
            throw new IllegalArgumentException("Sorry, input array is null or empty!");
        }

        boolean containsFirstNumber = false;
        boolean containsSecondNumber = false;

        for (int number : inputArray) {
            if (number == firstNumber) {
                containsFirstNumber = true;
            } else if (number == secondNumber) {
                containsSecondNumber = true;
            } else {
                return false;
            }
        }

        return containsFirstNumber && containsSecondNumber;
    }

}
