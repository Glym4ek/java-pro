package com.gmail.woosay333.fruitbox.box;

import com.gmail.woosay333.fruitbox.fruit.Fruit;

import java.util.ArrayList;
import java.util.List;

public class Box<T extends Fruit> {

    private final List<T> fruitBox;

    public Box() {
        fruitBox = new ArrayList<>();
    }

    public List<T> getFruitBox() {
        return fruitBox;
    }

    public boolean add(T item) {
        return fruitBox.add(item);
    }

    public boolean addAll(List<T> fruitList) {
        return fruitBox.addAll(fruitList);
    }

    public float getBoxWeight() {
        float totalWeight = 0;

        for (T item : fruitBox) {
            totalWeight += item.getWeight();
        }

        return totalWeight;
    }

    @Override
    public String toString() {
        StringBuilder box = new StringBuilder();

        for (Fruit fruit : fruitBox) {
            box.append(fruit).append(System.lineSeparator());
        }

        return box.toString().trim();
    }

    public <M extends Fruit> boolean compare(Box<M> anotherBox) {
        return this.getBoxWeight() == anotherBox.getBoxWeight();
    }

    public boolean merge(Box<T> anotherBox) {
        return this.addAll(anotherBox.getFruitBox());
    }

}
