package com.gmail.woosay333.filelogger;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.loggerfactory.LoggerFactory;

public class FileLoggerFactory implements LoggerFactory {

    private final FileLoggerConfiguration configuration;

    public FileLoggerFactory(String loggerFilePath, LoggerLevel loggerLevel, long fileMaxSize, String fileFormat) {
        this.configuration = new FileLoggerConfiguration(loggerFilePath, loggerLevel, fileMaxSize, fileFormat);
    }

    @Override
    public FileLogger getLogger() {
        return new FileLogger(configuration);
    }

    @Override
    public FileLoggerConfiguration getLoggerConfiguration() {
        return configuration;
    }

    public FileLoggerConfigurationLoader getLoggerConfigurationLoader() {
        return new FileLoggerConfigurationLoader();
    }

}
