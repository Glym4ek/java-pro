package com.gmail.woosay333.enums;

public enum LoggerLevel {

    DEBUG, INFO;

    public LoggerLevel[] getAllLoggerValues() {
        return LoggerLevel.values();
    }

}
