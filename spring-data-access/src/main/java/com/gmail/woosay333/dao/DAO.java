package com.gmail.woosay333.dao;

public interface DAO<T> {

    void add(T entity);

    void remove(int id);

    T getById(int id);

}
