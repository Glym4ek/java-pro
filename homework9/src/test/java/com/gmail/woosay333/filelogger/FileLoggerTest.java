package com.gmail.woosay333.filelogger;

import com.gmail.woosay333.enums.LoggerLevel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class FileLoggerTest {

    private static final String LOG_FILE_PATH = "src\\test\\java\\com\\gmail\\woosay333\\logs";
    private static final LoggerLevel LOG_LEVEL = LoggerLevel.INFO;
    private static final long FILE_MAX_SIZE = 1000;
    private static final String FILE_FORMAT = "log";

    private FileLogger logger;
    private FileLoggerConfiguration configuration;

    @BeforeEach
    void setUp() {
        configuration = new FileLoggerConfiguration(LOG_FILE_PATH, LOG_LEVEL, FILE_MAX_SIZE, FILE_FORMAT);
        logger = new FileLogger(configuration);
    }

    @Test
    void fileLoggerNotNullTest() {
        assertNotNull(logger);
    }

    @Test
    void fileLoggerConstructorTest() {
        assertAll(() -> assertEquals(LOG_FILE_PATH, configuration.getLogFilePath()),
                () -> assertEquals(LOG_LEVEL, configuration.getLoggerLevel()),
                () -> assertEquals(FILE_MAX_SIZE, configuration.getFileMaxSize()),
                () -> assertEquals(FILE_FORMAT, configuration.getFileFormat()),
                () -> assertNotNull(logger.getLogFile()));
    }

    @Test
    void fileLoggerDebugTest() {
        for (int i = 0; i < 10; i++) {
            logger.debug("test debug" + i);
        }

        var length = logger.getLogFile().length();
        assertTrue(length > 0);
    }

    @Test
    void fileLoggerInfoTest() {
        for (int i = 0; i < 10; i++) {
            logger.debug("test info" + i);
        }

        var length = logger.getLogFile().length();
        assertTrue(length > 0);
    }

    @AfterEach
    void tearDown() {
        logger = null;
        configuration = null;

        File file = new File(LOG_FILE_PATH);
        for (File f : Objects.requireNonNull(file.listFiles())) {
            if (!f.isDirectory())
                f.delete();
        }
    }

}
