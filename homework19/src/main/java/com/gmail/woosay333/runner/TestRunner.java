package com.gmail.woosay333.runner;

import com.gmail.woosay333.annotations.AfterSuite;
import com.gmail.woosay333.annotations.BeforeSuite;
import com.gmail.woosay333.annotations.Test;
import com.gmail.woosay333.exception.BeforeSuiteAfterSuiteAmountException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class TestRunner {

    private TestRunner() {
    }

    public static void start(Class<?> testClass) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        var testInstance = testClass.getDeclaredConstructor().newInstance();

        var beforeAmount = Arrays
                .stream(testInstance.getClass().getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(BeforeSuite.class))
                .count();

        var afterAmount = Arrays
                .stream(testInstance.getClass().getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(AfterSuite.class))
                .count();

        if (beforeAmount > 1 || afterAmount > 1) {
            throw new BeforeSuiteAfterSuiteAmountException("BeforeSuite of AfterSuite annotation amount more than one (BeforeSuite amount : " + beforeAmount + "; AfterSuite amount: " + afterAmount + ")");
        }

        var methodList = Arrays
                .stream(testInstance.getClass().getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(Test.class))
                .sorted((o1, o2) -> Integer.compare(o2.getAnnotation(Test.class).priority(), o1.getAnnotation(Test.class).priority()))
                .collect(Collectors.toCollection(LinkedList::new));

        var optionalBefore = Arrays
                .stream(testInstance.getClass().getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(BeforeSuite.class))
                .findFirst();
        var optionalAfter = Arrays
                .stream(testInstance.getClass().getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(AfterSuite.class))
                .findFirst();

        optionalBefore.ifPresent(methodList::addFirst);
        optionalAfter.ifPresent(methodList::addLast);

        for (Method method : methodList) {
            method.invoke(testInstance);
        }
    }

}
