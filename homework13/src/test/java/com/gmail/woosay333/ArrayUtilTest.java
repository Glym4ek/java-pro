package com.gmail.woosay333;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class ArrayUtilTest {

    private static Stream<Arguments> sortArguments() {
        return Stream.of(Arguments.of(new int[0], new int[0]),
                Arguments.of(new int[]{0}, new int[]{0}),
                Arguments.of(new int[]{-1, 1}, new int[]{-1, 1}),
                Arguments.of(new int[]{1, -1, -2}, new int[]{-2, -1, 1}),
                Arguments.of(new int[]{10, 7, 8, 9, 1, 5}, new int[]{1, 5, 7, 8, 9, 10}));
    }

    @ParameterizedTest
    @MethodSource("sortArguments")
    void quickSortMethodShouldSortInputArray(int[] input, int[] expected) {
        ArraySortUtil.quickSort(input, 0, input.length - 1);
        assertArrayEquals(expected, input);
    }

    @ParameterizedTest
    @MethodSource("sortArguments")
    void cocktailSortMethodShouldSortInputArray(int[] input, int[] expected) {
        ArraySortUtil.cocktailSort(input);
        assertArrayEquals(expected, input);
    }

}
