package com.gmail.woosay333.strategy;

import lombok.Value;

import static java.lang.Math.round;

@Value
public class Rectangle implements Figure {
    Double a;
    Double b;

    public Rectangle(Double a, Double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public Double getArea() {
        return round(a * b * 100.0) / 100.0;
    }

}
