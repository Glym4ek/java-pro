package com.gmail.woosay333.filelogger;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.loggerfactory.LoggerConfigurationLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.System.err;

public class FileLoggerConfigurationLoader implements LoggerConfigurationLoader {

    private static final String DEFAULT_LOGGER_FILE_PATH = "src\\main\\java\\com\\gmail\\woosay333\\logs";
    private static final LoggerLevel DEFAULT_LOGGER_LEVEL = LoggerLevel.DEBUG;
    private static final long DEFAULT_FILE_MAX_SIZE = 1000;
    private static final String DEFAULT_FILE_FORMAT = "log";


    @Override
    public FileLoggerConfiguration load(String configResourceFilePath) {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configResourceFilePath)) {
            Properties properties = new Properties();
            properties.load(inputStream);

            String logFilePath = properties.getProperty("logFilePath");
            LoggerLevel loggerLevel = LoggerLevel.valueOf(properties.getProperty("loggerLevel").toUpperCase());
            long fileMaxSize = Long.parseLong(properties.getProperty("fileMaxSize"));
            String fileFormat = properties.getProperty("fileFormat");

            return new FileLoggerConfiguration(logFilePath, loggerLevel, fileMaxSize, fileFormat);
        } catch (IOException | NullPointerException | IllegalArgumentException ex) {
            err.println("Can`t load some properties from configuration file! Load default settings.");
            return new FileLoggerConfiguration(
                    DEFAULT_LOGGER_FILE_PATH,
                    DEFAULT_LOGGER_LEVEL,
                    DEFAULT_FILE_MAX_SIZE,
                    DEFAULT_FILE_FORMAT);
        }
    }

}
