package com.gmail.woosay333.figure;

public interface Figure {

    double getArea();

}
