package com.gmail.woosay333;

import java.util.Objects;

import static java.lang.System.out;

public class SimpleBinaryTree<T extends Comparable<T>> {

    private Node<T> root;

    public Node<T> getRoot() {
        return root;
    }

    public void add(T value) {
        root = addRecursive(root, value);
    }

    private Node<T> addRecursive(Node<T> current, T value) {
        if (Objects.isNull(current)) {
            return new Node<>(value);
        }

        if (value.compareTo(current.getValue()) < 0) {
            current.setLeft(addRecursive(current.getLeft(), value));
        } else if ((value.compareTo(current.getValue()) > 0)) {
            current.setRight(addRecursive(current.getRight(), value));
        }

        return current;
    }

    public void traverseThrowSimpleTree() {
        traverseInOrder(root);
    }

    private void traverseInOrder(Node<T> node) {
        if (node != null) {
            traverseInOrder(node.getLeft());
            out.print(node.getValue() + " ");
            traverseInOrder(node.getRight());
        }
    }

}
