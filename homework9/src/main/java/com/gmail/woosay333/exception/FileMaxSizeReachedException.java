package com.gmail.woosay333.exception;

public class FileMaxSizeReachedException extends Exception {

    public FileMaxSizeReachedException() {
    }

    public FileMaxSizeReachedException(String message) {
        super(message);
    }

    public FileMaxSizeReachedException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileMaxSizeReachedException(Throwable cause) {
        super(cause);
    }

}
