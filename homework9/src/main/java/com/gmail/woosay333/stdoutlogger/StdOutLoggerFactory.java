package com.gmail.woosay333.stdoutlogger;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.loggerfactory.LoggerConfiguration;
import com.gmail.woosay333.loggerfactory.LoggerConfigurationLoader;
import com.gmail.woosay333.loggerfactory.LoggerFactory;

public class StdOutLoggerFactory implements LoggerFactory {

    private final StdOutLoggerConfiguration configuration;

    public StdOutLoggerFactory(LoggerLevel loggerLevel) {
        this.configuration = new StdOutLoggerConfiguration(loggerLevel);
    }

    @Override
    public StdOutLogger getLogger() {
        return new StdOutLogger(configuration);
    }

    @Override
    public LoggerConfiguration getLoggerConfiguration() {
        return configuration;
    }

    public LoggerConfigurationLoader getLoggerConfigurationLoader() {
        return new StdOutLoggerConfigurationLoader();
    }

}
