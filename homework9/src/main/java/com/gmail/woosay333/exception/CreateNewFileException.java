package com.gmail.woosay333.exception;

public class CreateNewFileException extends RuntimeException {

    public CreateNewFileException() {
    }

    public CreateNewFileException(String message) {
        super(message);
    }

    public CreateNewFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreateNewFileException(Throwable cause) {
        super(cause);
    }

}
