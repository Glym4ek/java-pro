package com.gmail.woosay333;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/messageServlet")
public class MessageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        var username = request.getParameter("username");
        var writer = response.getWriter();

        var htmlRespone = "<html>";
        htmlRespone += "<h2 style='text-align: center'>Your username is: " + username + "<br/>";
        htmlRespone += "</html>";

        writer.println(htmlRespone);
    }

}
