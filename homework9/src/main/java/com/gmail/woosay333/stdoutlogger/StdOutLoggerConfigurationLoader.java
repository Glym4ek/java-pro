package com.gmail.woosay333.stdoutlogger;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.loggerfactory.LoggerConfigurationLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.System.err;

public class StdOutLoggerConfigurationLoader implements LoggerConfigurationLoader {

    private static final LoggerLevel DEFAULT_LOGGER_LEVEL = LoggerLevel.DEBUG;

    @Override
    public StdOutLoggerConfiguration load(String configResourceFilePath) {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configResourceFilePath)) {
            Properties properties = new Properties();
            properties.load(inputStream);

            LoggerLevel loggerLevel = LoggerLevel.valueOf(properties.getProperty("loggerLevel").toUpperCase());

            return new StdOutLoggerConfiguration(loggerLevel);
        } catch (IOException | NullPointerException | IllegalArgumentException ex) {
            err.println("Can`t load some properties from configuration file! Load default settings.");
            return new StdOutLoggerConfiguration(
                    DEFAULT_LOGGER_LEVEL);
        }
    }

}
