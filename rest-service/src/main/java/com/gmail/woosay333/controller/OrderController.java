package com.gmail.woosay333.controller;

import com.gmail.woosay333.entity.Order;
import com.gmail.woosay333.service.OrderService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("/orders")
public class OrderController {

    private final OrderService service = new OrderService();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addOrder(Order order) {
        return Response.ok(service.addOrder(order)).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrder(@PathParam("id") UUID id) {
        return Response.ok(service.getOrder(id)).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrders() {
        return Response.ok(service.getOrders()).build();
    }

}
