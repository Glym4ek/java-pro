package com.gmail.woosay333.stdoutlogger;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.loggerfactory.Logger;

import static java.lang.System.out;

public class StdOutLogger implements Logger {

    private final StdOutLoggerConfiguration configuration;

    public StdOutLogger(StdOutLoggerConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void debug(String message) {
        log(LoggerLevel.DEBUG, message);
    }

    @Override
    public void info(String message) {
        log(LoggerLevel.INFO, message);
    }

    private void log(LoggerLevel level, String message) {
        String formattedMessage = getFormattedMessage(level, message);

        if (level.ordinal() <= configuration.getLoggerLevel().ordinal()) {
            out.println(formattedMessage);
        }
    }

}
