package com.gmail.woosay333;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ListOperationsRandomGenerators {

    private ListOperationsRandomGenerators() {
    }

    public static int[] getRandomIntArray(int arraySize, int minValue, int maxValue) throws NoSuchAlgorithmException {
        int[] array = new int[arraySize];
        Random random = SecureRandom.getInstanceStrong();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(maxValue - minValue) + minValue;
        }

        return array;
    }

    public static List<Integer> getRandomIntegerList(int listSize, int minValue, int maxValue) throws NoSuchAlgorithmException {
        int[] randomArray = getRandomIntArray(listSize, minValue, maxValue);
        return Arrays.stream(randomArray).boxed().collect(Collectors.toList());
    }

    public static List<String> getRandomStringList(int listSize) throws NoSuchAlgorithmException {
        List<String> strings = new ArrayList<>(listSize);
        Random random = SecureRandom.getInstanceStrong();
        String[] availableStrings = {"cat", "dog", "bird", "elephant", "tiger", "rabbit", "crocodile"};

        for (int i = 0; i < listSize; i++) {
            strings.add(availableStrings[random.nextInt(availableStrings.length)]);
        }

        return strings;
    }

}
