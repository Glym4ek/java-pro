package com.gmail.woosay333.competition;

import com.gmail.woosay333.competition.member.Member;
import com.gmail.woosay333.competition.member.Human;
import com.gmail.woosay333.competition.member.Cat;
import com.gmail.woosay333.competition.member.Robot;
import com.gmail.woosay333.competition.member.Util;
import com.gmail.woosay333.competition.obstacle.Obstacle;
import com.gmail.woosay333.competition.obstacle.Racetrack;
import com.gmail.woosay333.competition.obstacle.Wall;

public class CompetitionMain {

    public static void main(String[] args) {
        Member[] members = {
                new Human("Ron", 225, 5),
                new Cat("Garfield", 150, 3),
                new Robot("RoboCop", 2000, 1)
        };

        Obstacle[] obstacles = {
                new Racetrack("Racetrack 1", 30),
                new Wall("Wall 1", 1.5),
                new Racetrack("Racetrack 1", 30),
                new Wall("Wall 2", 2)
        };

        Util.getCompetitionResults(members, obstacles);
    }

}
