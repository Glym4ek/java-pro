package com.gmail.woosay333;

import static java.lang.System.out;

public class MyMathLibMain {

    public static void main(String[] args) {
        out.println(MyMathLib.getMaximum(1, 2));
        out.println(MyMathLib.getSquareRoot(16));
        out.println(MyMathLib.getPowerOfNumber(28, 4));
    }

}
