CREATE DATABASE IF NOT EXISTS homework30;

DROP USER IF EXISTS 'admin_homework30''@''localhost';

CREATE USER IF NOT EXISTS 'admin_homework30'@'localhost' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON homework30.* TO 'admin_homework30'@'localhost';

USE homework30;

DROP TABLE IF EXISTS Student;

CREATE TABLE IF NOT EXISTS Student
(
    id    INT PRIMARY KEY AUTO_INCREMENT,
    name  VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE
);