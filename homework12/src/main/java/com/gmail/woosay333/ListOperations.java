package com.gmail.woosay333;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import static java.lang.System.out;

public class ListOperations {

    private ListOperations() {
    }

    public static int countOccurrence(List<String> stringList, String stringToFind) {
        return Collections.frequency(stringList, stringToFind);
    }

    public static List<Integer> toList(int[] inputArray) {
        List<Integer> integers = new ArrayList<>();

        for (int element : inputArray) {
            integers.add(element);
        }

        return integers;
    }

    public static List<Integer> findUnique(List<Integer> integerList) {
        return integerList.stream().distinct().collect(Collectors.toList());
    }

    public static void calcOccurrence(List<String> stringList) {
        Set<String> unique = new HashSet<>(stringList);

        for (String key : unique) {
            out.println(key + ": " + Collections.frequency(stringList, key));
        }
    }

    public static List<Occurrence> findOccurrence(List<String> stringList) {
        Set<String> unique = new HashSet<>(stringList);
        List<Occurrence> occurrences = new ArrayList<>();

        for (String key : unique) {
            Occurrence occurrence = new Occurrence(key, Collections.frequency(stringList, key));
            occurrences.add(occurrence);
        }

        Collections.sort(occurrences);

        return occurrences;
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    public static class Occurrence implements Comparable<Occurrence> {

        private final String word;
        private final int wordCounter;

        @Override
        public int compareTo(Occurrence other) {
            return other.wordCounter - this.wordCounter;
        }

        @Override
        public String toString() {
            return String.format("{name: %s, occurrence: %s}", word, wordCounter);
        }
    }

}
