package com.gmail.woosay333.fruitbox.box;

import com.gmail.woosay333.fruitbox.fruit.Apple;
import com.gmail.woosay333.fruitbox.fruit.Orange;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class BoxTest {

    @Test
    void addMethodTest() {
        Box<Apple> appleBox = new Box<>();

        assertAll(() -> assertTrue(appleBox.add(new Apple())),
                () -> assertTrue(appleBox.add(new Apple())),
                () -> assertTrue(appleBox.add(new Apple())),
                () -> assertTrue(appleBox.add(new Apple())),
                () -> assertTrue(appleBox.add(new Apple())),
                () -> assertEquals(5, appleBox.getFruitBox().size()));
    }

    @Test
    void addAllMethodTest() {
        Box<Apple> appleBox = new Box<>();

        List<Apple> appleList = List.of(
                new Apple(),
                new Apple(),
                new Apple(),
                new Apple(),
                new Apple()
        );

        assertAll(() -> assertTrue(appleBox.addAll(appleList)),
                () -> assertEquals(5, appleBox.getFruitBox().size()));
    }

    @Test
    void getBoxWeightMethodShouldReturnTotalWeightOfAllFruitsInTheBox() {
        Box<Orange> orangeBox = new Box<>();
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());

        assertEquals(7.5F, orangeBox.getBoxWeight());
    }

    @Test
    void compareMethodShouldReturnTrueIfTotalWeightOfBoxesIsEquals() {
        Box<Apple> appleBox = new Box<>();
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());

        Box<Orange> orangeBox = new Box<>();
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());

        assertTrue(appleBox.compare(orangeBox));
    }

    @Test
    void compareMethodShouldReturnFalseIfTotalWeightOfBoxesIsNotEquals() {
        Box<Apple> appleBox = new Box<>();
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());

        Box<Orange> orangeBox = new Box<>();
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());
        orangeBox.add(new Orange());

        assertFalse(appleBox.compare(orangeBox));
    }

    @Test
    void mergeMethodShouldReturnTrueIfAllFruitsFromAnotherBoxWereOverflowing() {
        Box<Apple> appleBox = new Box<>();
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());
        appleBox.add(new Apple());

        Box<Apple> anotherAppleBox = new Box<>();
        anotherAppleBox.add(new Apple());
        anotherAppleBox.add(new Apple());
        anotherAppleBox.add(new Apple());
        anotherAppleBox.add(new Apple());

        assertAll(() -> assertTrue(appleBox.merge(anotherAppleBox)),
                () -> assertEquals(9, appleBox.getFruitBox().size()));
    }

}
