package com.gmail.woosay333.filelogger;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.loggerfactory.LoggerConfiguration;

public class FileLoggerConfiguration implements LoggerConfiguration {

    private final String logFilePath;
    private final LoggerLevel loggerLevel;
    private final long fileMaxSize;
    private final String fileFormat;

    public FileLoggerConfiguration(String logFilePath, LoggerLevel loggerLevel, long fileMaxSize, String fileFormat) {
        this.logFilePath = logFilePath;
        this.loggerLevel = loggerLevel;
        this.fileMaxSize = fileMaxSize;
        this.fileFormat = fileFormat;
    }

    @Override
    public String getLogFilePath() {
        return logFilePath;
    }

    @Override
    public LoggerLevel getLoggerLevel() {
        return loggerLevel;
    }

    @Override
    public long getFileMaxSize() {
        return fileMaxSize;
    }

    @Override
    public String getFileFormat() {
        return fileFormat;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "logFilePath='" + logFilePath + '\'' +
                ", loggerLevel=" + loggerLevel +
                ", fileMaxSize=" + fileMaxSize +
                ", fileFormat='" + fileFormat + '\'' +
                '}';
    }

}
