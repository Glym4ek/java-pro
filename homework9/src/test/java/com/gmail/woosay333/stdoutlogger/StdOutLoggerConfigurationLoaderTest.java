package com.gmail.woosay333.stdoutlogger;

import com.gmail.woosay333.enums.LoggerLevel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StdOutLoggerConfigurationLoaderTest {

    private static final String CONFIG_PATH = "configTest.properties";
    private static final String WRONG_CONFIG_PATH = "wrongConfigTest.properties";

    private StdOutLoggerConfigurationLoader configurationLoader;

    @BeforeEach
    void setUp() {
        configurationLoader = new StdOutLoggerConfigurationLoader();
    }

    @Test
    void loadShouldReturnConfigurationFromConfigFile() {
        StdOutLoggerConfiguration configuration = configurationLoader.load(CONFIG_PATH);

        assertTrue(configuration.getLoggerLevel() == LoggerLevel.INFO || configuration.getLoggerLevel() == LoggerLevel.DEBUG);
    }

    @Test
    void loadShouldReturnDefaultConfigurationIfFileNotFound() {
        StdOutLoggerConfiguration configuration = configurationLoader.load(WRONG_CONFIG_PATH);

        assertEquals(LoggerLevel.DEBUG, configuration.getLoggerLevel());
    }

    @AfterEach
    void tearDown() {
        configurationLoader = null;
    }

}
