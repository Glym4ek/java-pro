package com.gmail.woosay333.loggerfactory;

import com.gmail.woosay333.enums.LoggerLevel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public interface Logger {

    void debug(String message);

    void info(String message);

    default String getFormattedMessage(LoggerLevel loggerLevel, String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return String.format("[%s] [%s] Message: [%s]", dtf.format(now), loggerLevel.toString(), message);
    }

}
