package com.gmail.woosay333.competition.member;

import static java.lang.System.out;

public abstract class Member implements MemberBehavior {

    private final String name;
    private final double runLimit;
    private final double jumpLimit;
    private double totalDistance;
    private int countOfPassedObstacles;

    protected Member(String name, int runLimit, int jumpLimit) {
        if (runLimit < 0 || jumpLimit < 0) {
            throw new IllegalArgumentException("Invalid value of run or jump limits...");
        }
        this.name = name;
        this.runLimit = runLimit;
        this.jumpLimit = jumpLimit;
    }

    public String getName() {
        return name;
    }

    public double getRunLimit() {
        return runLimit;
    }

    public double getJumpLimit() {
        return jumpLimit;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void addPassedDistance(double totalDistance) {
        this.totalDistance += totalDistance;
    }

    public int getCountOfPassedObstacles() {
        return countOfPassedObstacles;
    }

    public void setCountOfPassedObstacles() {
        this.countOfPassedObstacles++;
    }

    @Override
    public void run() {
        out.println("Member " + name + " ran on a racetrack.");
    }

    @Override
    public void jump() {
        out.println("Member" + name + " ran an obstacle.");
    }

}
