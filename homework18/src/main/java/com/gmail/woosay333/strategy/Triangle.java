package com.gmail.woosay333.strategy;

import lombok.Value;

import static java.lang.Math.round;
import static java.lang.Math.sqrt;

@Value
public class Triangle implements Figure {
    Double a;
    Double b;
    Double c;

    @Override
    public Double getArea() {
        double half = (a + b + c) / 2;
        double area = sqrt(half * (half - a) * (half - b) * (half - c));

        return round(area * 100.0) / 100.0;
    }

}
