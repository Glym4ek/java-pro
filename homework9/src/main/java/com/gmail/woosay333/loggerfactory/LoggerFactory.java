package com.gmail.woosay333.loggerfactory;

public interface LoggerFactory {

    Logger getLogger();

    LoggerConfiguration getLoggerConfiguration();

    LoggerConfigurationLoader getLoggerConfigurationLoader();

}
