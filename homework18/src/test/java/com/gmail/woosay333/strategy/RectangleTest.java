package com.gmail.woosay333.strategy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RectangleTest {

    @Test
    void getAreaMethodShouldReturnAreaOfRectangle() {
        Figure rectangle = new Rectangle(5.0, 10.0);

        assertEquals(50.0, rectangle.getArea());
    }

}
