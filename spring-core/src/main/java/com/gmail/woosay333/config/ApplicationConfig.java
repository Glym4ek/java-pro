package com.gmail.woosay333.config;

import com.gmail.woosay333.model.Cart;
import com.gmail.woosay333.repository.ProductRepository;
import com.gmail.woosay333.service.ConsoleShoppingCartApplication;
import com.gmail.woosay333.service.ShoppingCartApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public ProductRepository productRepository() {
        return new ProductRepository("spring-core/src/main/resources/products.json");
    }

    @Bean
    public Cart cart() {
        return new Cart();
    }

    @Bean
    public ShoppingCartApplication shoppingCartApp(ProductRepository productRepository, Cart cart) {
        return new ShoppingCartApplication(productRepository, cart);
    }

    @Bean
    public ConsoleShoppingCartApplication consoleShoppingCartApp(ShoppingCartApplication shoppingCartApplication) {
        return new ConsoleShoppingCartApplication(shoppingCartApplication);
    }

}
