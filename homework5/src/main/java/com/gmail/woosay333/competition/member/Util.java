package com.gmail.woosay333.competition.member;

import com.gmail.woosay333.competition.obstacle.Obstacle;

import static java.lang.System.out;

public class Util {

    private Util() {
    }

    public static void getCompetitionResults(Member[] members, Obstacle[] obstacles) {
        for (Member member : members) {
            for (Obstacle obstacle : obstacles) {
                if (!obstacle.overcome(member)) {
                    out.println("Distance successfully covered by " + member.getName() + " = " + member.getTotalDistance() + " m. Obstacles successfully overcome by " + member.getName() + " = " + member.getCountOfPassedObstacles());
                    break;
                }
                out.println("Distance successfully covered by " + member.getName() + " = " + member.getTotalDistance() + " m. Obstacles successfully overcome by " + member.getName() + " = " + member.getCountOfPassedObstacles());
            }
        }
    }

}
