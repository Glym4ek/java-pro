package com.gmail.woosay333.car;

public class Car {

    public Car() {
        super();
    }

    public void start() {
        startElectricity();
        startCommand();
        startFuelSystem();
    }

    private void startFuelSystem() {
        System.out.println("Fuel system is starting...");
    }

    private void startCommand() {
        System.out.println("Command system is starting...");
    }

    private void startElectricity() {
        System.out.println("Electricity system is starting...");
    }
}
