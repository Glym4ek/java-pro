package com.gmail.woosay333.dao;

import java.util.Collection;

public interface DAO<T> {

    public void insert(T entity);

    public void update(T entity);

    public void delete(int id);

    public T getById(int id);

    public Collection<T> getAll();

}
