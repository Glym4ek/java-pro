package com.gmail.woosay333.product;

import com.gmail.woosay333.enums.ProductType;
import com.gmail.woosay333.exception.ProductNotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProductUtilTest {

    @ParameterizedTest
    @MethodSource("productArgs")
    @DisplayName("getProductList method should return list of given product type")
    void getProductListMethodTest(List<Product> products) {
        var expectedList = products.stream().filter(product -> product.getType().equals(ProductType.BOOK)).toList();

        assertAll(() -> assertInstanceOf(List.class, ProductUtil.getProductList(products, ProductType.BOOK)),
                () -> assertEquals(4, ProductUtil.getProductList(products, ProductType.BOOK).size()),
                () -> assertEquals(expectedList, ProductUtil.getProductList(products, ProductType.BOOK)));
    }

    @ParameterizedTest
    @MethodSource("productArgs")
    @DisplayName("getProductListWithDiscount method should return list of given product type and apply given discount to product price")
    void getProductListWithDiscountTest(List<Product> products) {
        var resultList = ProductUtil.getProductListWithDiscount(products, ProductType.BOOK, 30);
        var resultPriceList = resultList.stream().map(Product::getPrice).toList();
        var expectedList = List.of(70.0, 84.0, 63.0, 42.0);

        assertEquals(expectedList, resultPriceList);
    }

    @ParameterizedTest
    @MethodSource("productArgs")
    @DisplayName("findProductWithMinPrice method should throw ProductNotFoundException when product with given type not found")
    void findProductWithMinPriceThrowExceptionTest(List<Product> products) {
        products.removeIf(product -> product.getType().equals(ProductType.BOOK));

        assertThrows(ProductNotFoundException.class, () -> ProductUtil.findProductWithMinPrice(products, ProductType.BOOK));
    }

    @ParameterizedTest
    @MethodSource("productArgs")
    @DisplayName("findProductWithMinPrice method should return product with given type and minimum price")
    void findProductWithMinPriceTest(List<Product> products) throws ProductNotFoundException {
        var actualProduct = ProductUtil.findProductWithMinPrice(products, ProductType.BOOK);

        assertAll(() -> assertEquals(ProductType.BOOK, actualProduct.getType()),
                () -> assertEquals(60, actualProduct.getPrice()),
                () -> assertEquals(7, actualProduct.getId()));
    }

    @ParameterizedTest
    @MethodSource("productArgs")
    @DisplayName("getLastAddedProductsListTest method should return sorted list, witch come as method parameter")
    void getLastAddedProductsListReturnSameListTest(List<Product> products) {
        List<Product> expectedList = new ArrayList<>(products);
        expectedList.sort(Comparator.comparing(Product::getCreateDate));

        assertEquals(expectedList, ProductUtil.getLastAddedProductsList(products, 9));
    }

    @ParameterizedTest
    @MethodSource("productArgsWithThreadSleep")
    @DisplayName("getLastAddedProductsListTest method should return sorted list, witch contains last N added elements")
    void getLastAddedProductsListReturnListWithLastAddedNElementTest(List<Product> products) {
        List<Product> expectedList = new ArrayList<>();
        expectedList.add(products.get(products.size() - 1));
        expectedList.add(products.get(products.size() - 2));
        expectedList.add(products.get(products.size() - 3));

        expectedList.sort(Comparator.comparing(Product::getCreateDate));

        assertEquals(expectedList, ProductUtil.getLastAddedProductsList(products, 3));
    }

    @ParameterizedTest
    @MethodSource("productArgs")
    @DisplayName("getTotalPrice method should return total price of all products with given product type")
    void getTotalPriceMethodTest(List<Product> products) {
        assertEquals(250, ProductUtil.getTotalPrice(products, ProductType.BOOK, 2023, 100));
    }

    @ParameterizedTest
    @MethodSource("productArgs")
    @DisplayName("groupProductsByType method should return map of products grouping by product type")
    void groupProductsByTypeMethodTest(List<Product> products) {
        var resultMap = ProductUtil.groupProductsByType(products);

        assertAll(() -> assertEquals(3, resultMap.size()),
                () -> assertEquals(4, resultMap.get(ProductType.BOOK).size()),
                () -> assertEquals(3, resultMap.get(ProductType.JOURNAL).size()),
                () -> assertEquals(2, resultMap.get(ProductType.TOY).size()));
    }

    private static Stream<List<Product>> productArgs() {
        List<Product> products = new ArrayList<>();
        products.add(Product.newBuilder()
                .buildType(ProductType.BOOK)
                .buildPrice(100)
                .buildDiscount(true)
                .build());
        products.add(Product.newBuilder()
                .buildType(ProductType.JOURNAL)
                .buildPrice(150)
                .buildDiscount(true)
                .build());
        products.add(Product.newBuilder()
                .buildType(ProductType.BOOK)
                .buildPrice(120)
                .buildDiscount(true)
                .build());
        products.add(Product.newBuilder()
                .buildType(ProductType.BOOK)
                .buildPrice(90)
                .buildDiscount(true)
                .build());
        products.add(Product.newBuilder()
                .buildType(ProductType.TOY)
                .buildPrice(210)
                .buildDiscount(true)
                .build());
        products.add(Product.newBuilder()
                .buildType(ProductType.TOY)
                .buildPrice(240)
                .buildDiscount(true)
                .build());
        products.add(Product.newBuilder()
                .buildType(ProductType.BOOK)
                .buildPrice(60)
                .buildDiscount(true)
                .build());
        products.add(Product.newBuilder()
                .buildType(ProductType.JOURNAL)
                .buildPrice(150)
                .buildDiscount(true)
                .build());
        products.add(Product.newBuilder()
                .buildType(ProductType.JOURNAL)
                .buildPrice(180)
                .buildDiscount(true)
                .build());

        return Stream.of(products);
    }

    private static Stream<List<Product>> productArgsWithThreadSleep() throws InterruptedException {
        List<Product> products = new ArrayList<>();
        products.add(Product.newBuilder()
                .buildType(ProductType.BOOK)
                .buildPrice(100)
                .buildDiscount(true)
                .build());
        Thread.sleep(1000);
        products.add(Product.newBuilder()
                .buildType(ProductType.JOURNAL)
                .buildPrice(150)
                .buildDiscount(true)
                .build());
        Thread.sleep(1000);
        products.add(Product.newBuilder()
                .buildType(ProductType.BOOK)
                .buildPrice(120)
                .buildDiscount(true)
                .build());
        Thread.sleep(1000);
        products.add(Product.newBuilder()
                .buildType(ProductType.BOOK)
                .buildPrice(90)
                .buildDiscount(true)
                .build());
        Thread.sleep(1000);
        products.add(Product.newBuilder()
                .buildType(ProductType.TOY)
                .buildPrice(210)
                .buildDiscount(true)
                .build());
        Thread.sleep(1000);
        products.add(Product.newBuilder()
                .buildType(ProductType.TOY)
                .buildPrice(240)
                .buildDiscount(true)
                .build());
        Thread.sleep(1000);
        products.add(Product.newBuilder()
                .buildType(ProductType.BOOK)
                .buildPrice(60)
                .buildDiscount(true)
                .build());
        Thread.sleep(1000);
        products.add(Product.newBuilder()
                .buildType(ProductType.JOURNAL)
                .buildPrice(150)
                .buildDiscount(true)
                .build());
        Thread.sleep(1000);
        products.add(Product.newBuilder()
                .buildType(ProductType.JOURNAL)
                .buildPrice(180)
                .buildDiscount(true)
                .build());

        return Stream.of(products);
    }

}
