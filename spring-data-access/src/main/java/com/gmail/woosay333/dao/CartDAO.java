package com.gmail.woosay333.dao;

import com.gmail.woosay333.dto.Cart;
import com.gmail.woosay333.enums.Tables;
import com.gmail.woosay333.mapper.CartMapper;
import com.gmail.woosay333.mapper.ProductMapper;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Slf4j
@Value
@Repository
public class CartDAO implements DAO<Cart> {

    JdbcTemplate jdbcTemplate;

    @Autowired
    public CartDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void add(Cart entity) {
        String query = String.format("INSERT INTO %s VALUES ()", String.valueOf(Tables.CART).toLowerCase());

        log.info("Query: {}", query);

        jdbcTemplate.update(query);
    }

    @Override
    public void remove(int id) {
        String query = String.format("DELETE FROM %s WHERE id = ?", String.valueOf(Tables.CART).toLowerCase());

        log.info("Query: {}", query);

        jdbcTemplate.update(query, id);
    }

    @Override
    public Cart getById(int id) {
        String query = String.format("SELECT * FROM %s c JOIN %s p ON c.product_id = p.id WHERE c.cart_id = ?",
                String.valueOf(Tables.CART_PRODUCT).toLowerCase(),
                String.valueOf(Tables.PRODUCT).toLowerCase());

        log.info("Query: {}", query);

        return jdbcTemplate.queryForObject(query, new CartMapper(new ProductMapper()), id);
    }

    public void addToCart(int cartId, int productId) {
        String query = String.format("INSERT INTO %s (cart_id, product_id) VALUES (?, ?)",
                String.valueOf(Tables.CART_PRODUCT).toLowerCase());

        log.info("Query: {}", query);

        jdbcTemplate.update(query, cartId, productId);
    }

    public void removeFromCart(int cartId, int productId) {
        String query = String.format("DELETE FROM %s WHERE cart_id = (?) AND product_id = (?)",
                String.valueOf(Tables.CART_PRODUCT).toLowerCase());

        log.info("Query: {}", query);

        jdbcTemplate.update(query, cartId, productId);
    }

}
