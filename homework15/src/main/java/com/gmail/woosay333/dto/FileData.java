package com.gmail.woosay333.dto;

import lombok.Value;

@Value
public class FileData {

    String name;
    long size;
    String path;

}
