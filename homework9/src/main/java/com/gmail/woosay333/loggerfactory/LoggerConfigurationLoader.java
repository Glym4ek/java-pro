package com.gmail.woosay333.loggerfactory;

public interface LoggerConfigurationLoader {

    LoggerConfiguration load(String configFilePath);

}
