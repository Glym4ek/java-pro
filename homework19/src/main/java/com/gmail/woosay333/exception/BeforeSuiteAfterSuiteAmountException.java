package com.gmail.woosay333.exception;

public class BeforeSuiteAfterSuiteAmountException extends RuntimeException {

    public BeforeSuiteAfterSuiteAmountException(String message) {
        super(message);
    }

}
