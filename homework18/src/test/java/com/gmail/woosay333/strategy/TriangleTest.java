package com.gmail.woosay333.strategy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TriangleTest {

    @Test
    void getAreaMethodShouldReturnAreaOfTriangle() {
        Figure triangle = new Triangle(5.0,6.0,9.0);

        assertEquals(14.14, triangle.getArea());
    }

}
