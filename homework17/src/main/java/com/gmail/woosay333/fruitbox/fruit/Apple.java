package com.gmail.woosay333.fruitbox.fruit;

public class Apple extends Fruit {

    public Apple() {
        super("Apple", 1.0F);
    }

}
