package com.gmail.woosay333;

import com.gmail.woosay333.exception.NumberNotExistsException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ArrayOperationsTest {

    @ParameterizedTest
    @NullSource
    void getArrayTruncatedAfterLastGivenNumberShouldThrowIllegalArgumentExceptionWhenInputArrayIsNull(int[] argument) {
        String message = "Should throw illegal argument exception when input array is null!";
        assertThrows(IllegalArgumentException.class, () -> ArrayOperations.getArrayTruncatedAfterLastGivenNumber(argument, 4), message);
    }

    @ParameterizedTest
    @NullSource
    void checkArrayNumbersExistsShouldThrowIllegalArgumentExceptionWhenInputArrayIsNull(int[] argument) {
        String message = "Should throw illegal argument exception when input array is null!";
        assertThrows(IllegalArgumentException.class, () -> ArrayOperations.checkArrayNumbersExists(argument, 1, 4), message);
    }

    @ParameterizedTest
    @EmptySource
    void getArrayTruncatedAfterLastGivenNumberShouldThrowIllegalArgumentExceptionWhenInputArrayLengthIsZero(int[] argument) {
        String message = "Should throw illegal argument exception when input array length is zero!";
        assertThrows(IllegalArgumentException.class, () -> ArrayOperations.getArrayTruncatedAfterLastGivenNumber(argument, 4), message);
    }

    @ParameterizedTest
    @EmptySource
    void checkArrayNumbersExistsShouldThrowIllegalArgumentExceptionWhenInputArrayLengthIsZero(int[] argument) {
        String message = "Should throw illegal argument exception when input array length is zero!";
        assertThrows(IllegalArgumentException.class, () -> ArrayOperations.checkArrayNumbersExists(argument, 1, 4), message);
    }

    @ParameterizedTest
    @MethodSource("numbersProviderFactory")
    void shouldThrowMyRuntimeExceptionWhenInputArrayDoesNotExistsInputNumber(int argument) {
        int[] array = {1, 2, 4, 4, 2, 3, 4, 1, 7};
        assertThrows(NumberNotExistsException.class, () -> ArrayOperations.getArrayTruncatedAfterLastGivenNumber(array, argument), "Should throw my runtime exception when input array does not exists input number!");
    }

    @ParameterizedTest
    @MethodSource("argumentsForGetArrayTruncatedAfterLastGivenNumberMethod")
    void shouldReturnArrayTruncatedAfterLastGivenNumber(int[] inputArray, int number, int[] outputArray) {
        assertArrayEquals(outputArray, ArrayOperations.getArrayTruncatedAfterLastGivenNumber(inputArray, number));
    }

    @ParameterizedTest
    @MethodSource("argumentsForCheckArrayNumbersExists")
    void shouldReturnBooleanResultOfCheckArrayNumbersExists(int[] inputArray, boolean result) {
        assertEquals(result, ArrayOperations.checkArrayNumbersExists(inputArray, 1, 4));
    }

    private static IntStream numbersProviderFactory() {
        return IntStream.of(0, 5, -1, 8, 6, -2, -100, 100);
    }

    private static Stream<Arguments> argumentsForGetArrayTruncatedAfterLastGivenNumberMethod() {
        return Stream.of(Arguments.of(new int[]{1, 2, 4, 4, 2, 3, 4, 1, 7}, 1, new int[]{7}),
                Arguments.of(new int[]{1, 2, 4, 4, 2, 3, 4, 1, 7}, 2, new int[]{3, 4, 1, 7}),
                Arguments.of(new int[]{1, 2, 4, 4, 2, 3, 4, 1, 7}, 4, new int[]{1, 7}),
                Arguments.of(new int[]{1, 2, 4, 4, 2, 3, 4, 1, 7}, 3, new int[]{4, 1, 7}),
                Arguments.of(new int[]{1, 2, 4, 4, 2, 3, 4, 1, 7}, 7, new int[0]));
    }

    private static Stream<Arguments> argumentsForCheckArrayNumbersExists() {
        return Stream.of(Arguments.of(new int[]{1, 1, 1, 4, 4, 1, 4, 4}, true),
                Arguments.of(new int[]{1, 1, 1, 1, 1, 1}, false),
                Arguments.of(new int[]{4, 4, 4, 4}, false),
                Arguments.of(new int[]{1, 4, 4, 1, 1, 4, 3}, false));
    }

}
