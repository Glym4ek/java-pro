package com.gmail.woosay333.figure;

import static java.lang.Math.sqrt;

public class Triangle implements Figure {

    private final double a;
    private final double b;
    private final double c;

    public Triangle(double a, double b, double c) {
        if ((a + b) > c && (a + c) > b && (b + c) > a) {
            this.a = a;
            this.b = b;
            this.c = c;
        } else {
            throw new IllegalArgumentException("Triangle is not valid...");
        }
    }

    @Override
    public double getArea() {
        double p = (a + b + c) / 2.0;
        return sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @Override
    public String toString() {
        return "Triangle {" +
                "first side = " + a +
                ", second side = " + b +
                ", third side = " + c +
                '}';
    }

}
