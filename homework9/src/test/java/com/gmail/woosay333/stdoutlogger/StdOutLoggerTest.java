package com.gmail.woosay333.stdoutlogger;

import com.gmail.woosay333.enums.LoggerLevel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StdOutLoggerTest {

    private static final String MESSAGE_PATTERN = "\\[(?<timestamp>\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2})] \\[(?<level>INFO|ERROR|WARN|TRACE|DEBUG|FATAL)] Message: \\[(?<message>.*)]\\r\\n?|\\n";

    private StdOutLogger logger;

    @BeforeEach
    void setUp() {
        StdOutLoggerConfiguration configuration = new StdOutLoggerConfiguration(LoggerLevel.INFO);
        logger = new StdOutLogger(configuration);
    }

    @Test
    void stdOutLoggerConstructorTest() {
        assertNotNull(logger);
    }

    @ParameterizedTest
    @MethodSource("stringsArguments")
    void logDebugTest(String message) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        logger.debug(message);
        assertTrue(outputStream.toString().matches(MESSAGE_PATTERN));


    }

    @ParameterizedTest
    @MethodSource("stringsArguments")
    void logInfoTest(String message) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        logger.info(message);
        assertTrue(outputStream.toString().matches(MESSAGE_PATTERN));
    }

    @AfterEach
    void tearDown() {
        logger = null;
    }

    public static Stream<String> stringsArguments() {
        return Stream.of("test message", "", " ", "1234 test message 1234");
    }

}
