package com.gmail.woosay333.filelogger;

import com.gmail.woosay333.enums.LoggerLevel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FileLoggerConfigurationLoaderTest {

    private static final String CONFIG_PATH = "configTest.properties";
    private static final String WRONG_CONFIG_PATH = "wrongConfigTest.properties";

    @Test
    void loadMethodShouldReturnConfigurationFromConfigFile() {
        var configurationLoader = new FileLoggerConfigurationLoader();
        var configuration = configurationLoader.load(CONFIG_PATH);

        assertAll(() -> assertEquals("src\\test\\java\\com\\gmail\\woosay333\\logs", configuration.getLogFilePath()),
                () -> assertEquals(LoggerLevel.INFO, configuration.getLoggerLevel()),
                () -> assertEquals(1000, configuration.getFileMaxSize()),
                () -> assertEquals("log", configuration.getFileFormat()));
    }

    @Test
    void loadMethodShouldReturnDefaultConfigurationIfConfigFileNotFound() {
        var configurationLoader = new FileLoggerConfigurationLoader();
        var configuration = configurationLoader.load(WRONG_CONFIG_PATH);

        assertAll(() -> assertEquals("src\\main\\java\\com\\gmail\\woosay333\\logs", configuration.getLogFilePath()),
                () -> assertEquals(LoggerLevel.DEBUG, configuration.getLoggerLevel()),
                () -> assertEquals(1000, configuration.getFileMaxSize()),
                () -> assertEquals("log", configuration.getFileFormat()));
    }

}
