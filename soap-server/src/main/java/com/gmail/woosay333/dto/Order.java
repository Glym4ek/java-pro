package com.gmail.woosay333.dto;

import javax.xml.bind.annotation.XmlElement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {
    private static int idGenerator = 0;

    private final int id;
    private final Date date = new Date();
    private double cost;

    @XmlElement
    private final List<Product> products;

    public Order() {
        this.id = ++idGenerator;
        this.products = new ArrayList<>();
    }

    public Order(List<Product> products) {
        this.id = ++idGenerator;
        this.products = products;
        this.cost = getCost();
    }

    public int getId() {
        return id;
    }

    public double getCost() {
        cost = products.stream().mapToDouble(Product::getCost).sum();
        return cost;
    }

    public List<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        return "Order {" +
                "id = " + id +
                ", date = " + simpleDateFormat.format(date) +
                ", cost = " + cost +
                ", products = " + products +
                '}';
    }

}
