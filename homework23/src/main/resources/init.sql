CREATE DATABASE IF NOT EXISTS homework23;

CREATE USER 'admin_homework23'@'localhost' IDENTIFIED BY '0000';

GRANT ALL PRIVILEGES ON homework23.* TO 'admin_homework23'@'localhost';

USE homework23;

CREATE TABLE IF NOT EXISTS Homework
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    name        VARCHAR(255) NOT NULL DEFAULT 'unknown',
    description TEXT         NOT NULL
);

CREATE TABLE IF NOT EXISTS Lesson
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    name        VARCHAR(255) NOT NULL DEFAULT 'unknown',
    updatedAt   TIMESTAMP             DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    homework_id INT          NOT NULL,
    FOREIGN KEY (homework_id) REFERENCES Homework (id)
);

CREATE TABLE IF NOT EXISTS Schedule
(
    id        INT PRIMARY KEY AUTO_INCREMENT,
    name      VARCHAR(255) NOT NULL DEFAULT 'unknown',
    updatedAt TIMESTAMP             DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS Lesson_Schedule
(
    lesson_id   INT NOT NULL,
    schedule_id INT NOT NULL,
    PRIMARY KEY (lesson_id, schedule_id),
    FOREIGN KEY (lesson_id) REFERENCES Lesson (id),
    FOREIGN KEY (schedule_id) REFERENCES Schedule (id)
);