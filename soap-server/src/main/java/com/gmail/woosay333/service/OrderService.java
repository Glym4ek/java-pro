package com.gmail.woosay333.service;

import com.gmail.woosay333.dto.Order;
import com.gmail.woosay333.repository.OrderRepository;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public class OrderService {
    private final OrderRepository orderRepository = new OrderRepository();

    @WebMethod
    public boolean add(Order order) {
        return orderRepository.addOrder(order);
    }

    @WebMethod
    public Order getById(int id) {
        return orderRepository.getOrderById(id);
    }

    @WebMethod
    public List<Order> getAll() {
        return orderRepository.getAllOrders();
    }

}
