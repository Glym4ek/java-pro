package com.gmail.woosay333.dao;

import com.gmail.woosay333.config.JdbcConfig;
import com.gmail.woosay333.dto.Cart;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CartDAOTest {

    private static AnnotationConfigApplicationContext context;
    private static CartDAO cartDAO;
    //private static ProductDAO productDAO;

    @BeforeAll
    static void setUp() {
        context = new AnnotationConfigApplicationContext(JdbcConfig.class);
        JdbcTemplate jdbcTemplate = context.getBean(JdbcTemplate.class);
        cartDAO = context.getBean(CartDAO.class);
        //productDAO = context.getBean(ProductDAO.class);

        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS homework32.product " +
                "(id INT PRIMARY KEY AUTO_INCREMENT, " +
                "name VARCHAR(255) NOT NULL, " +
                "price DECIMAL(8, 2))");

        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS homework32.cart " +
                "(id INT PRIMARY KEY AUTO_INCREMENT)");

        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS homework32.cart_product " +
                "(cart_id INT NOT NULL, " +
                "product_id INT NOT NULL, " +
                "FOREIGN KEY (cart_id) REFERENCES homework32.cart (id), " +
                "FOREIGN KEY (product_id) REFERENCES homework32.product (id))");

        jdbcTemplate.update("INSERT INTO homework32.product (name, price) " +
                "VALUES " +
                "('test product 1', 150.00), " +
                "('test product 2', 210.20)");
    }

    @Order(1)
    @Test
    void addCartTest() {
        Cart cart = Cart.builder().build();

        cartDAO.add(cart);
    }

    @Order(2)
    @Test
    void deleteCartTest() {
        Cart cart = Cart.builder().build();

        cartDAO.add(cart);
        cartDAO.remove(2);
    }

    @Order(3)
    @Test
    void addProductToCartTest() {
        cartDAO.addToCart(1, 1);
    }

    @Order(4)
    @Test
    void getCartByIdTest() {
        assertEquals(1, cartDAO.getById(1).getId());
    }

    @Order(5)
    @Test
    void removeProductFromCartTest() {
        cartDAO.removeFromCart(1, 1);
    }


    @AfterAll
    static void tearDown() {
        JdbcTemplate jdbcTemplate = context.getBean(JdbcTemplate.class);

        jdbcTemplate.update("DROP TABLE homework32.cart_product");
        jdbcTemplate.update("DROP TABLE homework32.cart");
        jdbcTemplate.update("DROP TABLE homework32.product");

        //productDAO = null;
        context = null;
    }

}
