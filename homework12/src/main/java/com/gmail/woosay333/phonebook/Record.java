package com.gmail.woosay333.phonebook;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Record {

    private String name;
    private String number;

}
