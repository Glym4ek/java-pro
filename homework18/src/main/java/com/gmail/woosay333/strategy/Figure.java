package com.gmail.woosay333.strategy;

public interface Figure {

    Double getArea();

}
