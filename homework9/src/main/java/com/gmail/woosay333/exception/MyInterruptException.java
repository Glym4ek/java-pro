package com.gmail.woosay333.exception;

public class MyInterruptException extends RuntimeException {

    public MyInterruptException() {
    }

    public MyInterruptException(String message) {
        super(message);
    }

    public MyInterruptException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyInterruptException(Throwable cause) {
        super(cause);
    }

}
