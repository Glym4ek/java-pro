package com.gmail.woosay333.dao;

import com.gmail.woosay333.dto.Homework;
import com.gmail.woosay333.dto.Lesson;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LessonDao implements Dao<Lesson> {

    @Override
    public List<Lesson> getAll() throws SQLException {
        String query = "SELECT homework.Lesson.id, " +
                "homework.Lesson.name, " +
                "homework.Homework.id, " +
                "homework.Homework.name, " +
                "homework.Homework.description " +
                "FROM homework.Lesson " +
                "JOIN homework.Homework ON homework.Lesson.homework_id = homework.Homework.id";

        List<Lesson> lessonList = new ArrayList<>();

        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                Lesson lesson = Lesson.builder()
                        .id(resultSet.getInt("id"))
                        .name(resultSet.getString("name"))
                        .homework(Homework.builder()
                                .id(resultSet.getInt("id"))
                                .name(resultSet.getString("name"))
                                .description(resultSet.getString("description"))
                                .build())
                        .build();

                lessonList.add(lesson);
            }
        } catch (SQLException ex) {
            throw new SQLException("Get all lessons operation failed!", ex);
        }

        return lessonList;
    }

    @Override
    public int add(Lesson entity) throws SQLException {
        String query = "INSERT INTO homework.Lesson (name, homework_id) VALUES (?, ?)";
        int affectedRows;

        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getHomework().getId());

            affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating lesson failed, no rows affected!");
            }

            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    affectedRows = resultSet.getInt(1);
                } else {
                    throw new SQLException("Creating lesson failed, no ID obtained!");
                }
            }
        }

        return affectedRows;
    }

    @Override
    public void remove(int id) throws SQLException {
        String query = "DELETE FROM homework.Lesson WHERE id = ?";

        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public Optional<Lesson> get(int id) throws SQLException {
        String query = "SELECT homework.Lesson.id, " +
                "homework.Lesson.name, " +
                "homework.Homework.id, " +
                "homework.Homework.name, " +
                "homework.Homework.description " +
                "FROM homework.lesson " +
                "JOIN homework.Homework ON homework.Lesson.id = homework.Homework.id " +
                "WHERE homework.Lesson.id = ?";

        try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Lesson lesson = Lesson.builder()
                        .id(resultSet.getInt("id"))
                        .name(resultSet.getString("name"))
                        .homework(Homework.builder()
                                .id(resultSet.getInt("id"))
                                .name(resultSet.getString("name"))
                                .description(resultSet.getString("description"))
                                .build())
                        .build();

                return Optional.of(lesson);
            }
        }

        return Optional.empty();
    }

}
