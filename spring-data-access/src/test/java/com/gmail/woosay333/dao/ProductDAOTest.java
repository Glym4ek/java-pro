package com.gmail.woosay333.dao;

import com.gmail.woosay333.config.JdbcConfig;
import com.gmail.woosay333.dto.Product;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ProductDAOTest {

    private static AnnotationConfigApplicationContext context;
    private static ProductDAO productDAO;

    @BeforeAll
    static void setUp() {
        context = new AnnotationConfigApplicationContext(JdbcConfig.class);
        productDAO = context.getBean(ProductDAO.class);
        JdbcTemplate jdbcTemplate = context.getBean(JdbcTemplate.class);

        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS homework32.product " +
                "(id INT PRIMARY KEY AUTO_INCREMENT, " +
                "name VARCHAR(255) NOT NULL, " +
                "price DECIMAL(8, 2))");

        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS homework32.cart " +
                "(id INT PRIMARY KEY AUTO_INCREMENT)");

        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS homework32.cart_product " +
                "(cart_id INT NOT NULL, " +
                "product_id INT NOT NULL, " +
                "FOREIGN KEY (cart_id) REFERENCES homework32.cart (id), " +
                "FOREIGN KEY (product_id) REFERENCES homework32.product (id))");
    }

    @Order(1)
    @Test
    void addProductTest() {
        Product product = Product.builder()
                .name("Test product 1")
                .price(542.25)
                .build();

        productDAO.add(product);

        assertEquals(1, productDAO.getAll().size());
    }

    @Order(2)
    @Test
    void deleteProductTest() {
        Product product = Product.builder()
                .name("Test product 2")
                .price(600.00)
                .build();

        productDAO.add(product);
        productDAO.remove(2);

        assertEquals(1, productDAO.getAll().size());
    }

    @Order(3)
    @Test
    void getProductByIdTest() {
        Product product = Product.builder()
                .name("Test product 3")
                .price(800.25)
                .build();

        productDAO.add(product);
        assertEquals("Test product 3", productDAO.getById(3).getName());
    }

    @Order(4)
    @Test
    void getAllProductsTest() {
        Product product1 = Product.builder()
                .name("Test product 4")
                .price(470)
                .build();

        Product product2 = Product.builder()
                .name("Test product 5")
                .price(1250)
                .build();

        productDAO.add(product1);
        productDAO.add(product2);

        assertEquals(4, productDAO.getAll().size());
    }


    @AfterAll
    static void tearDown() {
        JdbcTemplate jdbcTemplate = context.getBean(JdbcTemplate.class);

        jdbcTemplate.update("DROP TABLE homework32.cart_product");
        jdbcTemplate.update("DROP TABLE homework32.cart");
        jdbcTemplate.update("DROP TABLE homework32.product");

        productDAO = null;
        context = null;
    }

}
