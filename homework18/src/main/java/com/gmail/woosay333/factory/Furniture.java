package com.gmail.woosay333.factory;

import java.math.BigDecimal;

public class Furniture {

    private final String name;
    private final BigDecimal price;

    protected Furniture(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " {" +
                "name = '" + name + '\'' +
                ", price = " + price +
                '}';
    }

}
