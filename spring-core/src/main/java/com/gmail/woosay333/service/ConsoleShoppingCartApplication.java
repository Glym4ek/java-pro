package com.gmail.woosay333.service;

import java.util.Scanner;

import static java.lang.System.out;

public class ConsoleShoppingCartApplication {

    private final ShoppingCartApplication shoppingCartApplication;
    private final Scanner scanner;

    public ConsoleShoppingCartApplication(ShoppingCartApplication shoppingCartApplication) {
        this.shoppingCartApplication = shoppingCartApplication;
        scanner = new Scanner(System.in);
    }

    public void start() {
        do {
            displayMenu();
            String choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    addToCart();
                    break;
                case "2":
                    removeFromCart();
                    break;
                case "3":
                    printCartInfo();
                    break;
                case "4":
                    out.println("Exiting the program.");
                    return;
                default:
                    out.println("Invalid choice. Please try again.");
            }
        } while (true);
    }

    private void displayMenu() {
        out.println("=== Shopping Cart Menu ===");
        out.println("1. Add product to cart");
        out.println("2. Remove product from cart");
        out.println("3. Display cart contents");
        out.println("4. Exit");
        out.print("Enter your choice: ");
    }

    private void addToCart() {
        out.print("Enter product ID: ");
        int id = Integer.parseInt(scanner.nextLine());
        shoppingCartApplication.addToCart(id);
    }

    private void removeFromCart() {
        out.print("Enter product ID: ");
        int id = Integer.parseInt(scanner.nextLine());
        shoppingCartApplication.removeFromCart(id);
    }

    private void printCartInfo() {
        shoppingCartApplication.printCartInfo();
    }

}
