package com.gmail.woosay333.product;

import com.gmail.woosay333.enums.ProductType;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
public class Product {

    private int id;
    private ProductType type;
    private double price;
    private boolean isDiscount;
    private final LocalDateTime createDate = LocalDateTime.now();

    private Product() {
    }

    public static ProductBuilder newBuilder() {
        return new Product().new ProductBuilder();
    }

    @Override
    public String toString() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        return "Product {" +
                "id = " + id +
                ", type = " + type +
                ", price = " + price +
                ", discount = " + isDiscount +
                ", createDate = " + dateTimeFormatter.format(createDate) +
                '}';
    }

    public class ProductBuilder {

        private static int idGenerator = 0;

        private ProductBuilder() {
            id = ++idGenerator;
        }

        public ProductBuilder buildId(int id) {
            Product.this.id = id;
            return this;
        }

        public ProductBuilder buildType(ProductType type) {
            Product.this.type = type;
            return this;
        }

        public ProductBuilder buildPrice(double price) {
            Product.this.price = price;
            return this;
        }

        public ProductBuilder buildDiscount(boolean isDiscount) {
            Product.this.isDiscount = isDiscount;
            return this;
        }

        public Product build() {
            return Product.this;
        }
    }

}
