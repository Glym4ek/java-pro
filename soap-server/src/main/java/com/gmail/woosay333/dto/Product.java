package com.gmail.woosay333.dto;

public class Product {
    private static int idGenerator = 0;

    private final int id;
    private String name;
    private double cost;

    public Product() {
        this.id = ++idGenerator;
    }

    public Product(String name, double cost) {
        this.id = ++idGenerator;
        this.name = name;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Product {" +
                "id = " + id +
                ", name = '" + name + '\'' +
                ", cost = " + cost +
                '}';
    }

}
