package com.gmail.woosay333.filelogger;

import com.gmail.woosay333.enums.LoggerLevel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileLoggerConfigurationTest {

    private FileLoggerConfiguration configuration;
    private static final String LOG_FILE_PATH = "src\\test\\resources\\configTest.properties";
    private static final LoggerLevel LOG_LEVEL = LoggerLevel.INFO;
    private static final long FILE_MAX_SIZE = 1000;
    private static final String FILE_FORMAT = "log";

    @BeforeEach
    public void setUp() {
        configuration = new FileLoggerConfiguration(LOG_FILE_PATH, LOG_LEVEL, FILE_MAX_SIZE, FILE_FORMAT);
    }

    @Test
    void loggerConfigurationShouldBeNotNull() {
        assertNotNull(configuration);
    }

    @Test
    void checkFileLoggerConfigurationConstructor() {
        assertAll(() -> assertEquals(LOG_FILE_PATH, configuration.getLogFilePath()),
                () -> assertEquals(LOG_LEVEL, configuration.getLoggerLevel()),
                () -> assertEquals(FILE_MAX_SIZE, configuration.getFileMaxSize()),
                () -> assertEquals(FILE_FORMAT, configuration.getFileFormat()));
    }

    @AfterEach
    void tearDown() {
        configuration = null;
    }

}
