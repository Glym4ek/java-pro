package com.gmail.woosay333.factory;

import com.gmail.woosay333.factory.exception.CreateFactoryException;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

public class FurnitureFactory {

    private FurnitureFactory() {
    }

    public static <T extends Furniture> T getFurniture(Class<T> furnitureType, String name, BigDecimal price) {
        try {
            return furnitureType.getDeclaredConstructor(String.class, BigDecimal.class).newInstance(name, price);
        } catch (NoSuchMethodException
                 | InvocationTargetException
                 | IllegalAccessException
                 | InstantiationException ex) {
            throw new CreateFactoryException("Can`t return furniture of type " + furnitureType.getSimpleName());
        }
    }

}
