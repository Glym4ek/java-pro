package com.gmail.woosay333.coffee.order;

import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;
import java.util.Queue;

import static java.lang.System.out;

@Slf4j
public class CoffeeOrderBoard {

    private final Queue<Order> orders;
    private long ordersCounter;

    public CoffeeOrderBoard() {
        orders = new LinkedList<>();
    }

    public boolean add(String name) {
        if (name == null || name.isBlank()) {
            log.error("Client name is null or empty");
            return false;
        }
        log.info("Adding order order to queue for client {}", name);
        return orders.add(new Order(++ordersCounter, name));
    }

    public void deliver() {
        if (!orders.isEmpty()) {
            Order order = orders.poll();
            log.info("Issuing an order for no {} for client {}", order.getOrderNumber(), order.getClientName());
        } else {
            log.warn("Queue is empty!");
        }
    }

    public void deliver(long id) {
        if (id <= 0) {
            log.error("Order id is less than one");
            return;
        }

        Order order = null;

        for (Order o : orders) {
            if (o.getOrderNumber() == id) {
                order = o;
                orders.remove(o);
                log.debug("Order with id {} was removed from queue", id);
                break;
            }
        }

        if (order != null) {
            log.info("Issuing an order for no {} for client {}", order.getOrderNumber(), order.getClientName());
        } else {
            log.error("Order with id = {} not found", id);
        }
    }

    public void draw() {
        if (!orders.isEmpty()) {
            log.info("Printing current state of queue:");
            orders.forEach(order -> out.println(order.getOrderNumber() + " | " + order.getClientName()));
        } else {
            log.warn("Queue is empty");
        }
    }

}
