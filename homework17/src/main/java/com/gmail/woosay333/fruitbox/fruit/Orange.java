package com.gmail.woosay333.fruitbox.fruit;

public class Orange extends Fruit {

    public Orange() {
        super("Orange", 1.5F);
    }

}
