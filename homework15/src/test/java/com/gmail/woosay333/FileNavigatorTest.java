package com.gmail.woosay333;

import com.gmail.woosay333.dto.FileData;
import com.gmail.woosay333.filenavigator.FileNavigator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FileNavigatorTest {

    private FileNavigator fileNavigator;

    @BeforeEach
    void setUp() {
        fileNavigator = new FileNavigator();
    }

    @Test
    void fileNavigatorInstanceNotNull() {
        assertNotNull(fileNavigator);
    }

    @Test
    void fileNavigatorContainerIsNotNull() {
        assertNotNull(fileNavigator.getContainer());
    }

    @Test
    void addMethodShouldThrowIllegalArgumentExceptionWhenPathsAreNotEquals() {
        assertThrows(IllegalArgumentException.class, () -> fileNavigator.add("testPath/test", new FileData("testFile", 1000, "testPath/wrongPath")));
    }

    @Test
    void addMethodShouldAddNewEntityToTheContainer() {
        FileData fileData1 = new FileData("file1.doc", 250, "path/test1");
        FileData fileData2 = new FileData("file2.pdf", 550, "path/test1");
        FileData fileData3 = new FileData("file1.pdf", 320, "path/test2");
        FileData fileData4 = new FileData("file2.doc", 350, "path/test2");

        fileNavigator.add(fileData1.getPath(), fileData1);
        fileNavigator.add(fileData2.getPath(), fileData2);
        fileNavigator.add(fileData3.getPath(), fileData3);
        fileNavigator.add(fileData4.getPath(), fileData4);

        assertAll(() -> assertEquals(2, fileNavigator.getContainer().size()),
                () -> assertTrue(fileNavigator.getContainer().containsKey(fileData1.getPath())),
                () -> assertTrue(fileNavigator.getContainer().containsKey(fileData3.getPath())),
                () -> assertTrue(fileNavigator.getContainer().get(fileData1.getPath()).contains(fileData1)),
                () -> assertTrue(fileNavigator.getContainer().get(fileData2.getPath()).contains(fileData2)),
                () -> assertTrue(fileNavigator.getContainer().get(fileData3.getPath()).contains(fileData3)),
                () -> assertTrue(fileNavigator.getContainer().get(fileData4.getPath()).contains(fileData4)));
    }

    @Test
    void findMethodShouldReturnEmptySetIfGivenPathNotExistsInContainer() {
        FileData fileData1 = new FileData("file1.doc", 250, "path/test1");
        FileData fileData2 = new FileData("file2.pdf", 550, "path/test1");
        FileData fileData3 = new FileData("file1.pdf", 320, "path/test2");
        FileData fileData4 = new FileData("file2.doc", 350, "path/test2");

        fileNavigator.add(fileData1.getPath(), fileData1);
        fileNavigator.add(fileData2.getPath(), fileData2);
        fileNavigator.add(fileData3.getPath(), fileData3);
        fileNavigator.add(fileData4.getPath(), fileData4);

        assertEquals(0, fileNavigator.find("path/test3").size());
    }

    @Test
    void findMethodShouldReturnSetOfFilesByGivenPath() {
        FileData fileData1 = new FileData("file1.doc", 250, "path/test1");
        FileData fileData2 = new FileData("file2.pdf", 550, "path/test1");

        fileNavigator.add(fileData1.getPath(), fileData1);
        fileNavigator.add(fileData2.getPath(), fileData2);

        assertAll(() -> assertTrue(fileNavigator.find(fileData1.getPath()).contains(fileData1)),
                () -> assertTrue(fileNavigator.find(fileData2.getPath()).contains(fileData2)));
    }

    @Test
    void removeMethodShouldRemoveEntityFromContainerByGivenPath() {
        FileData fileData1 = new FileData("file1.doc", 250, "path/test1");
        FileData fileData2 = new FileData("file2.pdf", 550, "path/test1");

        fileNavigator.add(fileData1.getPath(), fileData1);
        fileNavigator.add(fileData2.getPath(), fileData2);

        fileNavigator.remove(fileData1.getPath());

        assertTrue(fileNavigator.getContainer().isEmpty());
    }

    @Test
    void filterBySizeMethodTest() {
        FileData fileData1 = new FileData("file1.doc", 250, "path/test1");
        FileData fileData2 = new FileData("file2.pdf", 550, "path/test1");
        FileData fileData3 = new FileData("file3.pdf", 320, "path/test1");
        FileData fileData4 = new FileData("file4.doc", 350, "path/test1");

        fileNavigator.add(fileData1.getPath(), fileData1);
        fileNavigator.add(fileData2.getPath(), fileData2);
        fileNavigator.add(fileData3.getPath(), fileData3);
        fileNavigator.add(fileData4.getPath(), fileData4);

        assertAll(() -> assertEquals(3, fileNavigator.filterBySize(fileData1.getPath(), 500).size()),
                () -> assertTrue(fileNavigator.filterBySize(fileData1.getPath(), 500).contains(fileData1)),
                () -> assertTrue(fileNavigator.filterBySize(fileData3.getPath(), 500).contains(fileData3)),
                () -> assertTrue(fileNavigator.filterBySize(fileData4.getPath(), 500).contains(fileData4)));
    }

    @Test
    void sortBySizeMethodShouldReturnSetSortedByFileSize() {
        FileData fileData1 = new FileData("file1.doc", 250, "path/test1");
        FileData fileData2 = new FileData("file2.pdf", 550, "path/test1");
        FileData fileData3 = new FileData("file3.pdf", 320, "path/test1");
        FileData fileData4 = new FileData("file4.doc", 350, "path/test1");

        fileNavigator.add(fileData1.getPath(), fileData1);
        fileNavigator.add(fileData2.getPath(), fileData2);
        fileNavigator.add(fileData3.getPath(), fileData3);
        fileNavigator.add(fileData4.getPath(), fileData4);

        Set<FileData> sortedSet = fileNavigator.sortBySize("path/test1");
        Set<FileData> expectedSet = new TreeSet<>(Comparator.comparingLong(FileData::getSize));
        expectedSet.add(fileData1);
        expectedSet.add(fileData2);
        expectedSet.add(fileData3);
        expectedSet.add(fileData4);

        assertEquals(expectedSet, sortedSet);
    }

    @AfterEach
    void tearDown() {
        fileNavigator = null;
    }

}
