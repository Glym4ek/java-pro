package com.gmail.woosay333.runner;

import com.gmail.woosay333.exception.BeforeSuiteAfterSuiteAmountException;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TestRunnerTest {

    @Test
    void startMethodShouldThrowExceptionWhenNumberOfBeforeSuiteOfAfterSuiteMoreThanOne() {
        assertThrows(BeforeSuiteAfterSuiteAmountException.class, () -> TestRunner.start(SecondTestCase.class));
    }

    @Test
    void startMethodShouldRunAllMethodsWithBeforeSuiteTestAfterSuiteAnnotations() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream outTemp = System.out;
        System.setOut(new PrintStream(out));

        String expected = "Init" +
                System.lineSeparator() +
                "Test 1" +
                System.lineSeparator() +
                "Test 2" +
                System.lineSeparator() +
                "Test 3" +
                System.lineSeparator() +
                "Test 4" +
                System.lineSeparator() +
                "Destroy" +
                System.lineSeparator();

        TestRunner.start(FirstTestCase.class);

        assertEquals(expected, out.toString());

        System.setOut(outTemp);
    }

    @Test
    void startMethodShouldRunAllMethodsWithTestAnnotations() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream outTemp = System.out;
        System.setOut(new PrintStream(out));

        String expected = "Test 1" +
                System.lineSeparator() +
                "Test 2" +
                System.lineSeparator() +
                "Test 3" +
                System.lineSeparator() +
                "Test 4" +
                System.lineSeparator();

        TestRunner.start(ThirdTestCase.class);

        assertEquals(expected, out.toString());

        System.setOut(outTemp);
    }

}
