package com.gmail.woosay333;

import com.gmail.woosay333.enums.LoggerLevel;
import com.gmail.woosay333.filelogger.FileLoggerFactory;
import com.gmail.woosay333.loggerfactory.Logger;
import com.gmail.woosay333.loggerfactory.LoggerConfiguration;
import com.gmail.woosay333.loggerfactory.LoggerConfigurationLoader;
import com.gmail.woosay333.loggerfactory.LoggerFactory;
import com.gmail.woosay333.stdoutlogger.StdOutLoggerFactory;

import static java.lang.System.out;

public class Main {
    public static void main(String[] args) {
//        Test file logger output
        LoggerFactory factory = new FileLoggerFactory("src\\main\\java\\com\\gmail\\woosay333\\logs", LoggerLevel.INFO, 1000, "log");
        LoggerConfigurationLoader configurationLoader = factory.getLoggerConfigurationLoader();
        LoggerConfiguration configuration = configurationLoader.load("config.properties");
        out.println(configuration);
        Logger logger = factory.getLogger();


        for (int i = 0; i < 40; i++) {
            logger.debug("debug message " + i);
            logger.info("info message " + i);
        }

//        Test console logger output
        factory = new StdOutLoggerFactory(LoggerLevel.INFO);
        logger = factory.getLogger();

        out.println(configuration);

        for (int i = 0; i < 40; i++) {
            logger.debug("debug message " + i);
            logger.info("info message " + i);
        }
    }

}
