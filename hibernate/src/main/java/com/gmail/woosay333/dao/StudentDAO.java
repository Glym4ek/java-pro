package com.gmail.woosay333.dao;

import com.gmail.woosay333.config.HibernateSession;
import com.gmail.woosay333.entity.Student;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.hibernate.query.criteria.JpaCriteriaQuery;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
public class StudentDAO implements DAO<Student> {

    @Override
    public void insert(Student entity) {
        log.info("Trying to insert student: {}", entity);

        try (Session session = HibernateSession.INSTANCE.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(entity);
            transaction.commit();

            log.info("Student successfully inserted: {}", entity);
        } catch (HibernateException ex) {
            log.error("Error occurred while inserting student: {}", entity, ex.getCause());
        }
    }

    @Override
    public void update(Student entity) {
        log.info("Trying to updating student: {}", entity);

        try (Session session = HibernateSession.INSTANCE.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.merge(entity);
            transaction.commit();

            log.info("Student successfully updated");
        } catch (HibernateException ex) {
            log.error("Error occurred while updating student: {}", entity, ex.getCause());
        }
    }

    @Override
    public void delete(int id) {
        log.info("Trying to delete student with id: {}", id);

        try (Session session = HibernateSession.INSTANCE.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(session.get(Student.class, id));
            transaction.commit();

            log.info("Student successfully deleted");
        } catch (HibernateException ex) {
            log.error("Error occurred while delete student with id: {}", id, ex.getCause());
        }
    }

    @Override
    public Student getById(int id) {
        log.info("Trying to get student with id: {}", id);

        try (Session session = HibernateSession.INSTANCE.openSession()) {
            Student student = session.get(Student.class, id);

            return Optional.ofNullable(student).orElseThrow(() -> new HibernateException("Error occurred while getting student with id: " + id));
        } catch (HibernateException ex) {
            log.error("Error occurred while getting student with id: {}", id, ex.getCause());
            return null;
        }
    }

    @Override
    public Collection<Student> getAll() {
        log.info("Trying to get all students");

        try (Session session = HibernateSession.INSTANCE.openSession()) {
            HibernateCriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            JpaCriteriaQuery<Student> query = criteriaBuilder.createQuery(Student.class);
            query.from(Student.class);
            List<Student> resultList = session.createQuery(query).getResultList();
            log.info("Student were find successfully: {}", resultList);

            return resultList;
        } catch (HibernateException ex) {
            log.error("Error occurred while getting all student", ex.getCause());
            return Collections.emptyList();
        }
    }

}
