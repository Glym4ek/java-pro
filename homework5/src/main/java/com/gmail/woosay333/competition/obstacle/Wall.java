package com.gmail.woosay333.competition.obstacle;

import com.gmail.woosay333.competition.member.Member;

import static java.lang.System.out;

public class Wall extends Obstacle {

    private final double height;

    public Wall(String name, double height) {
        super(name);
        if (height <= 0) {
            throw new IllegalArgumentException("Invalid value of racetrack height...");
        }
        this.height = height;
    }

    @Override
    public boolean overcome(Member member) {
        member.jump();

        if (member.getJumpLimit() >= height) {
            out.println("Participant " + member.getName() + " overcame " + getName() + " " + height + " m.");
            member.setCountOfPassedObstacles();
            return true;
        } else {
            out.println("Participant " + member.getName() + " didn't overcome " + getName() + " " + height + " m.");
            return false;
        }
    }

}
