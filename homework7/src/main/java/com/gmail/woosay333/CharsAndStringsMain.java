package com.gmail.woosay333;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.Scanner;

import static java.lang.System.out;

public class CharsAndStringsMain {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        out.println("~~~~~~~~ Test findSymbolOccurance method ~~~~~~~~");
        out.printf("Number of char `%s` in string `%s` is: %d %n", 's', "some string to find number of characters", findSymbolOccurance("some string to find number of characters", 's'));
        out.println();

        out.println("~~~~~~~~ Test findWordPosition method ~~~~~~~~");
        out.println(findWordPosition("Apollo", "pollo"));
        out.println(findWordPosition("Apple", "Plant"));
        out.println();

        out.println("~~~~~~~~ Test stringReverse method ~~~~~~~~");
        out.println(stringReverse("Test string to reverse"));
        out.println();

        out.println("~~~~~~~~ Test isPalindrome method ~~~~~~~~");
        out.println(isPalindrome("ERE"));
        out.println(isPalindrome("Allo"));
        out.println();

        out.println("~~~~~~~~ Test guessTheWord method ~~~~~~~~");
        String[] words = {"apple", "orange", "lemon", "banana", "apricot",
                "avocado", "broccoli", "carrot", "cherry", "garlic",
                "grape", "melon", "leak", "kiwi", "mango",
                "mushroom", "nut", "olive", " pea", "peanut", "pear",
                "pepper", "pineapple", "pumpkin", "potato"};
        guessTheWord(words);
    }

    public static int findSymbolOccurance(String source, char symbolToFind) {
        int counter = 0;

        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == symbolToFind) {
                counter += 1;
            }
        }

        return counter;
    }

    public static int findWordPosition(String source, String target) {
        return source.indexOf(target);
    }

    public static String stringReverse(String source) {
        return new StringBuilder(source).reverse().toString();
    }

    public static boolean isPalindrome(String source) {
        int i = 0;
        int j = source.length() - 1;

        while (i < j) {
            if (source.charAt(i++) != source.charAt(j--)) {
                return false;
            }
        }

        return true;
    }

    public static void guessTheWord(String[] wordsArray) throws NoSuchAlgorithmException {
        Random randomNumber = SecureRandom.getInstanceStrong();
        String randomWord = wordsArray[randomNumber.nextInt(wordsArray.length)];
        Scanner scanner = new Scanner(System.in);

        StringBuilder hiddenWord = new StringBuilder().append("#".repeat(15));
        String userInputWord;

        do {
            out.printf("Secret word is: %s%n", randomWord);
            out.println("Please, input your word");
            userInputWord = scanner.nextLine().toLowerCase().trim();

            if (userInputWord.equals(randomWord)) {
                out.printf("Congratulations, you guessed the word %s%n", randomWord);
                return;
            } else {
                StringBuilder partiallyGuessedWord = new StringBuilder(hiddenWord);

                for (int i = 0; i < Math.min(userInputWord.length(), randomWord.length()); i++) {
                    if (userInputWord.charAt(i) == randomWord.charAt(i)) {
                        partiallyGuessedWord.setCharAt(i, userInputWord.charAt(i));
                    }
                }

                hiddenWord = new StringBuilder(partiallyGuessedWord.toString());
                out.printf("Part of the word you guessed: %s%n", hiddenWord);
            }
        } while (true);
    }

}
