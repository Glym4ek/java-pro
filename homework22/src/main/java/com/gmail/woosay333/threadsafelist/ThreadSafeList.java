package com.gmail.woosay333.threadsafelist;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ThreadSafeList<T> {

    private final List<T> list;

    public ThreadSafeList() {
        this.list = new CopyOnWriteArrayList<>();
    }

    public void add(T element) {
        list.add(element);
    }

    public void add(int index, T element) {
        list.add(index, element);
    }

    public boolean remove(T element) {
        return list.remove(element);
    }

    public T remove(int index) {
        return list.remove(index);
    }

    public T get(int index) {
        return list.get(index);
    }

    public int size() {
        return list.size();
    }

    @Override
    public String toString() {
        return list.toString();
    }

}
