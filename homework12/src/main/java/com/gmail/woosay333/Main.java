package com.gmail.woosay333;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import static java.lang.System.out;

public class Main {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        out.println("~~~~~~Task1~~~~~~");
        String wordToCountOccurrence = "elephant";
        List<String> words = ListOperationsRandomGenerators.getRandomStringList(20);
        out.println("Input list: " + words);
        out.println("Word " + wordToCountOccurrence + " occurrence: " + ListOperations.countOccurrence(words, wordToCountOccurrence));

        out.println("~~~~~~Task2~~~~~~");
        int[] ints = ListOperationsRandomGenerators.getRandomIntArray(20, -100, 100);
        out.println("Type of input: " + ints.getClass().getSimpleName());
        List<Integer> intList = ListOperations.toList(ints);
        out.println("Type of output: " + intList.getClass().getSimpleName());

        out.println("~~~~~~Task3~~~~~~");
        List<Integer> integerList = ListOperationsRandomGenerators.getRandomIntegerList(20, -5, 5);
        List<Integer> uniqueIntegerList = ListOperations.findUnique(integerList);
        out.println("Input integer list: " + integerList);
        out.println("Unique integer list: " + uniqueIntegerList);

        out.println("~~~~~~Task4~~~~~~");
        out.println("Input list: " + words);
        ListOperations.calcOccurrence(words);

        out.println("~~~~~~Task5~~~~~~");
        out.println("Input list: " + words);
        List<ListOperations.Occurrence> occurrence = ListOperations.findOccurrence(words);
        out.println(occurrence);
    }

}
