package com.gmail.woosay333.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CarBuilderTest {

    @Test
    void carBuilderTest() {
        CarBuilder carBuilder = new CarBuilderImpl();

        Car car = carBuilder.buildEngine("ROTARY ENGINE")
                .buildTransmission("AUTOMATIC")
                .buildSeats(5)
                .buildNavigator("FUNCTIONAL")
                .buildColor("RED")
                .getCar();

        assertAll(() -> assertEquals("ROTARY ENGINE", car.getEngine()),
                () -> assertEquals("AUTOMATIC", car.getTransmission()),
                () -> assertEquals(5, car.getSeats()),
                () -> assertEquals("FUNCTIONAL", car.getNavigator()),
                () -> assertEquals("RED", car.getColor()));
    }

}
