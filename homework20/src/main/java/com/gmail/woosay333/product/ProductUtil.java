package com.gmail.woosay333.product;

import com.gmail.woosay333.enums.ProductType;
import com.gmail.woosay333.exception.ProductNotFoundException;
import lombok.NonNull;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class ProductUtil {

    private ProductUtil() {
    }

    public static List<Product> getProductList(@NonNull List<Product> products, @NonNull ProductType type) {
        return products.stream()
                .filter(product -> product.getType().equals(type))
                .toList();
    }

    public static List<Product> getProductListWithDiscount(@NonNull List<Product> products, @NonNull ProductType type, double discount) {
        return products.stream()
                .filter(product -> product.getType().equals(type) && product.isDiscount())
                .map(product -> Product.newBuilder()
                        .buildId(product.getId())
                        .buildType(product.getType())
                        .buildPrice(product.getPrice() - product.getPrice() / 100 * discount)
                        .buildDiscount(false)
                        .build())
                .toList();
    }

    public static Product findProductWithMinPrice(@NonNull List<Product> products, @NonNull ProductType type) throws ProductNotFoundException {
        return products.stream()
                .filter(t -> t.getType().equals(type))
                .min(Comparator.comparingDouble(Product::getPrice))
                .orElseThrow(() -> new ProductNotFoundException(String.format("Product [category: %s] not found", type)));
    }

    public static List<Product> getLastAddedProductsList(@NonNull List<Product> products, long amount) {
        long mountToSkip = products.size() - amount;

        return mountToSkip >= 0
                ? products.stream().sorted(Comparator.comparing(Product::getCreateDate)).skip(products.size() - amount).toList()
                : products.stream().sorted(Comparator.comparing(Product::getCreateDate)).toList();

    }

    public static double getTotalPrice(@NonNull List<Product> products, @NonNull ProductType type, int creationYear, double maxPrice) {
        return products.stream()
                .filter(product -> product.getType().equals(type))
                .filter(product -> product.getCreateDate().getYear() == creationYear)
                .filter(product -> product.getPrice() <= maxPrice)
                .flatMapToDouble(product -> DoubleStream.of(product.getPrice()))
                .sum();
    }

    public static Map<ProductType, List<Product>> groupProductsByType(@NonNull List<Product> products) {
        return products.stream().collect(Collectors.groupingBy(Product::getType));
    }

}
