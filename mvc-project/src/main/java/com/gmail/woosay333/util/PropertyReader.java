package com.gmail.woosay333.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.System.err;

public class PropertyReader {

    private PropertyReader() {
    }

    public static Properties getProperties(String path) {
        Properties properties = new Properties();

        try (InputStream inputStream = PropertyReader.class.getClassLoader().getResourceAsStream(path)) {
            properties.load(inputStream);
        } catch (IOException ex) {
            err.println("No such property file found with path: " + path);
        }

        return properties;
    }

}
