package com.gmail.woosay333.stdoutlogger;

import com.gmail.woosay333.enums.LoggerLevel;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StdOutLoggerConfigurationTest {

    @ParameterizedTest
    @MethodSource("loggerLevelArguments")
    void loggerLevelTest(LoggerLevel expected, LoggerLevel actual) {
        StdOutLoggerConfiguration configuration = new StdOutLoggerConfiguration(actual);

        assertEquals(expected, configuration.getLoggerLevel());
    }

    public static Stream<Arguments> loggerLevelArguments() {
        return Stream.of(Arguments.of(LoggerLevel.DEBUG, LoggerLevel.DEBUG),
                Arguments.of(LoggerLevel.INFO, LoggerLevel.INFO));
    }

}
