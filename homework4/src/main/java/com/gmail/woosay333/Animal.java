package com.gmail.woosay333;

public abstract class Animal {

    private static int counterOfCreatedClasses = 0;
    private String name;

    protected Animal(String name) {
        this.name = name;
        ++counterOfCreatedClasses;
    }

    public static int getCounterOfCreatedClasses() {
        return counterOfCreatedClasses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void run(int obstacleLength);

    public abstract void swim(int obstacleLength);

    @Override
    public String toString() {
        return "Animal {" +
                "name: " + name + '\'' +
                '}';
    }

}
