package com.gmail.woosay333.runner;

import com.gmail.woosay333.annotations.AfterSuite;
import com.gmail.woosay333.annotations.BeforeSuite;
import com.gmail.woosay333.annotations.Test;

import static java.lang.System.out;

public class SecondTestCase {

    @BeforeSuite
    void init() {
        out.println("Init");
    }

    @BeforeSuite
    void init2() {
        out.println("Init2");
    }

    @Test(priority = 9)
    void startTest1() {
        out.println("Test 1");
    }

    @Test(priority = 8)
    void startTest2() {
        out.println("Test 2");
    }

    @Test(priority = 7)
    void startTest3() {
        out.println("Test 3");
    }

    @Test(priority = 6)
    void startTest4() {
        out.println("Test 4");
    }

    @AfterSuite
    void destroy() {
        out.println("Destroy");
    }

}
