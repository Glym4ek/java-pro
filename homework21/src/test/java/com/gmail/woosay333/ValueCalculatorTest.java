package com.gmail.woosay333;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValueCalculatorTest {

    @Test
    @DisplayName("Size of inner array should be 1_000_000 if constructor parameter `size` less than 1_000_000")
    void innerArraySizeWithIncorrectConstructorValueTest() {
        ValueCalculator vc = new ValueCalculator(999_999);

        assertEquals(1_000_000, vc.getArray().length);
    }

    @Test
    @DisplayName("Size of inner array should be equal with constructor parameter `size`")
    void innerArraySizeWithCorrectConstructorValueTest() {
        ValueCalculator vc = new ValueCalculator(1_000_001);

        assertEquals(1_000_001, vc.getArray().length);
    }

    @Test
    @DisplayName("CalculateTimeArrayOperation method should calculate print in console time spent from the start to the end of the program")
    void calculateTimeArrayOperationTest() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream temp = System.out;
        System.setOut(new PrintStream(out));

        ValueCalculator vc = new ValueCalculator(10_000_000);
        vc.calculateTimeArrayOperation(1, 20);

        assertAll(() -> assertEquals(vc.getArray()[0], (float) (1.0 * Math.sin(0.2f + 0.0 / 5) * Math.cos(0.2f + 0.0 / 5) * Math.cos(0.4f + 0.0 / 2))),
                () -> assertTrue(out.toString().contains("Operation with 20 threads takes ")));

        System.setOut(temp);
    }

}
