package com.gmail.woosay333.config;

import com.gmail.woosay333.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public enum HibernateSession {

    INSTANCE;

    private final SessionFactory sessionFactory;

    HibernateSession() {
        sessionFactory = new Configuration().configure()
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
    }

    public Session openSession() {
        return sessionFactory.openSession();
    }

}
