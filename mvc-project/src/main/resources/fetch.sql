-- Retrieving all Homework records
SELECT *
FROM homework.Homework;


-- Retrieving all Lesson records, including Homework data
SELECT homework.Lesson.*,
       homework.Homework.name        AS homework_name,
       homework.Homework.description AS homework_description
FROM homework.Lesson
         INNER JOIN homework.Homework ON homework.Lesson.homework_id = homework.Homework.id;


-- Get all Lesson records (including Homework data) sorted by update time
SELECT homework.Lesson.*,
       homework.Homework.name        AS homework_name,
       homework.Homework.description AS homework_description
FROM homework.Lesson
         INNER JOIN homework.Homework ON homework.Lesson.homework_id = homework.Homework.id
ORDER BY homework.Lesson.updatedAt DESC;


-- Retrieving all Schedule entries, including Lesson data
SELECT homework.Schedule.*,
       homework.Lesson.name      AS lesson_name,
       homework.Lesson.updatedAt AS lesson_updateAt
FROM homework.Schedule
         INNER JOIN homework.Lesson_Schedule ON homework.Schedule.id = homework.Lesson_Schedule.schedule_id
         INNER JOIN homework.Lesson ON homework.Lesson_Schedule.lesson_id = homework.Lesson.id;


-- Getting the number of Lessons for each Schedule
SELECT homework.Schedule.*,
       COUNT(homework.Lesson_Schedule.lesson_id) AS lesson_count
FROM homework.Schedule
         LEFT JOIN homework.Lesson_Schedule ON homework.Schedule.id = homework.Lesson_Schedule.schedule_id
GROUP BY homework.Schedule.id;