package com.gmail.woosay333;

import lombok.SneakyThrows;

import java.util.Arrays;

import static java.lang.System.out;

public class ValueCalculator {

    private final double[] array;

    public ValueCalculator(int size) {
        if (size < 1_000_000) {
            this.array = new double[1_000_000];
        } else {
            this.array = new double[size];
        }
    }

    public double[] getArray() {
        return array;
    }

    @SneakyThrows
    public void calculateTimeArrayOperation(double fillArrayNumber, int numberOfSubArrays) {
        long start = System.currentTimeMillis();

        fillArray(fillArrayNumber);

        double[][] dividedArray = divideIntoSubArrays(array, numberOfSubArrays);

        Thread[] workers = new Thread[numberOfSubArrays];

        for (int i = 0; i < dividedArray.length; i++) {
            workers[i] = new Thread(new RunnableImpl(dividedArray[i]));
            workers[i].start();
        }

        for (Thread worker : workers) {
            worker.join();
        }

        copyMatrixToArray(dividedArray);

        long end = System.currentTimeMillis();

        out.printf("Operation with %d threads takes %d ms.%n", numberOfSubArrays, end - start);
    }

    private double[][] divideIntoSubArrays(double[] array, int numberOfSubArrays) {
        double bucketSize = array.length / (float) numberOfSubArrays;
        double[][] result = new double[numberOfSubArrays][];

        for (int currentBucket = 0; currentBucket < numberOfSubArrays; currentBucket++) {
            result[currentBucket] = Arrays.copyOfRange(array, (int) Math.ceil(currentBucket * bucketSize), (int) Math.ceil(currentBucket * bucketSize + bucketSize));
        }

        return result;
    }

    private void fillArray(double fillArrayNumber) {
        Arrays.fill(array, fillArrayNumber);
    }

    private void copyMatrixToArray(double[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            double[] row = matrix[i];
            for (int j = 0; j < row.length; j++) {
                double number = matrix[i][j];
                array[i * row.length + j] = number;
            }
        }
    }

    private static class RunnableImpl implements Runnable {

        private final double[] array;

        public RunnableImpl(double[] array) {
            this.array = array;
        }

        @Override
        public void run() {
            for (int i = 0; i < array.length; i++) {
                array[i] = (float) (array[i] * Math.sin(0.2f + (float) i / 5) * Math.cos(0.2f + (float) i / 5) * Math.cos(0.4f + (float) i / 2));
            }
        }
    }

}
