DROP DATABASE IF EXISTS homework;

CREATE DATABASE IF NOT EXISTS homework;

CREATE USER IF NOT EXISTS 'admin_homework'@'localhost' IDENTIFIED BY '0000';

GRANT ALL PRIVILEGES ON homework.* TO 'admin_homework'@'localhost';

CREATE TABLE IF NOT EXISTS homework.Homework
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    name        VARCHAR(255) NOT NULL DEFAULT 'unknown',
    description TEXT         NOT NULL
);

CREATE TABLE IF NOT EXISTS homework.Lesson
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    name        VARCHAR(255) NOT NULL DEFAULT 'unknown',
    updatedAt   TIMESTAMP             DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    homework_id INT          NOT NULL,
    FOREIGN KEY (homework_id) REFERENCES Homework (id)
);

CREATE TABLE IF NOT EXISTS homework.Schedule
(
    id        INT PRIMARY KEY AUTO_INCREMENT,
    name      VARCHAR(255) NOT NULL DEFAULT 'unknown',
    updatedAt TIMESTAMP             DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS homework.Lesson_Schedule
(
    lesson_id   INT NOT NULL,
    schedule_id INT NOT NULL,
    PRIMARY KEY (lesson_id, schedule_id),
    FOREIGN KEY (lesson_id) REFERENCES Lesson (id),
    FOREIGN KEY (schedule_id) REFERENCES Schedule (id)
);