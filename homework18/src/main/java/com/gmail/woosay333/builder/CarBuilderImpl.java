package com.gmail.woosay333.builder;

public class CarBuilderImpl implements CarBuilder {
    private String engine;
    private String transmission;
    private int seats;
    private String navigator;
    private String color;

    @Override
    public CarBuilder buildEngine(String engine) {
        this.engine = engine;
        return this;
    }

    @Override
    public CarBuilder buildTransmission(String transmission) {
        this.transmission = transmission;
        return this;
    }

    @Override
    public CarBuilder buildSeats(int seats) {
        this.seats = seats;
        return this;
    }

    @Override
    public CarBuilder buildNavigator(String navigator) {
        this.navigator = navigator;
        return this;
    }

    @Override
    public CarBuilder buildColor(String color) {
        this.color = color;
        return this;
    }

    @Override
    public Car getCar() {
        return new Car(engine, transmission, seats, navigator, color);
    }

}
