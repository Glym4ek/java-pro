package com.gmail.woosay333.builder;

public interface CarBuilder {

    CarBuilder buildEngine(String engine);

    CarBuilder buildTransmission(String transmission);

    CarBuilder buildSeats(int seats);

    CarBuilder buildNavigator(String navigator);

    CarBuilder buildColor(String color);

    Car getCar();

}
