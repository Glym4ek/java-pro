package com.gmail.woosay333.threadsafelist;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThreadSafeListTest {

    @Test
    void threadSafeListTest() throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        ThreadSafeList<Integer> threadSafeList = new ThreadSafeList<>();

        Stream.<Runnable>of(
                () -> {
                    for (int i = 0; i < 100; i++) {
                        threadSafeList.add(i);
                    }

                    threadSafeList.remove(0);
                },
                () -> {
                    for (int i = 100; i < 200; i++) {
                        threadSafeList.add(i);
                    }

                    threadSafeList.remove(0);
                },
                () -> {
                    for (int i = 200; i < 300; i++) {
                        threadSafeList.add(i);
                    }

                    threadSafeList.remove(0);
                }
        ).forEach(threadPool::submit);

        threadPool.shutdown();
        threadPool.awaitTermination(1L, TimeUnit.MINUTES);

        assertEquals(297, threadSafeList.size());
    }

}
