package com.gmail.woosay333.competition.member;

public interface MemberBehavior {

    void run();

    void jump();

}
