package com.gmail.woosay333.loggerfactory;

import com.gmail.woosay333.enums.LoggerLevel;

public interface LoggerConfiguration {

    LoggerLevel getLoggerLevel();

    default String getLogFilePath() {
        throw  new UnsupportedOperationException();
    }

    default long getFileMaxSize() {
        throw  new UnsupportedOperationException();
    }

    default String getFileFormat() {
        throw  new UnsupportedOperationException();
    }

}
