package com.gmail.woosay333.factory;

import java.math.BigDecimal;

public class Sofa extends Furniture {

    public Sofa(String name, BigDecimal price) {
        super(name, price);
    }

}
