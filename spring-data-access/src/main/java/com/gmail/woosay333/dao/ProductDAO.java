package com.gmail.woosay333.dao;

import com.gmail.woosay333.dto.Product;
import com.gmail.woosay333.enums.Tables;
import com.gmail.woosay333.mapper.ProductMapper;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Value
@Repository
public class ProductDAO implements DAO<Product> {

    JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void add(Product product) {
        String query = String.format("INSERT INTO %s (name, price) VALUES (?, ?)", String.valueOf(Tables.PRODUCT).toLowerCase());

        log.info("Query: {}", query);

        jdbcTemplate.update(query, product.getName(), product.getPrice());
    }

    @Override
    public void remove(int id) {
        String query = String.format("DELETE FROM %s WHERE id = ?", String.valueOf(Tables.PRODUCT).toLowerCase());

        log.info("Query: {}", query);

        jdbcTemplate.update(query, id);
    }

    @Override
    public Product getById(int id) {
        String query = String.format("SELECT * FROM %s WHERE id = ?", String.valueOf(Tables.PRODUCT).toLowerCase());

        log.info("Query: {}", query);

        return jdbcTemplate.queryForObject(query, new ProductMapper(), id);
    }

    public List<Product> getAll() {
        String query = String.format("SELECT * FROM %s", String.valueOf(Tables.PRODUCT).toLowerCase());

        log.info("Query: {}", query);

        return jdbcTemplate.query(query, new ProductMapper());
    }

}
