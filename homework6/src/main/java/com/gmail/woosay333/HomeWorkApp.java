package com.gmail.woosay333;

import static java.lang.System.out;

public class HomeWorkApp {

    private static final String DELIMITER = "~~~~~~~~~~~~~~~~~~~";

    public static void main(String[] args) {
        printThreeWords();
        out.println(DELIMITER);

        checkSumSign(5, -5);
        out.println(DELIMITER);

        printColor(1);
        out.println(DELIMITER);

        compareNumbers(1, 1);
        out.println(DELIMITER);

        out.println(compareSumOfTwoNumbers(4, 5));
        out.println(compareSumOfTwoNumbers(5, 5));
        out.println(compareSumOfTwoNumbers(5, 15));
        out.println(compareSumOfTwoNumbers(5, 16));
        out.println(DELIMITER);

        numberIsPositiveOrNegative(-1);
        numberIsPositiveOrNegative(-0);
        numberIsPositiveOrNegative(1);
        out.println(DELIMITER);

        out.println(isNumberNegative(-1));
        out.println(isNumberNegative(0));
        out.println(DELIMITER);

        printStringFewTimes("Test string", 3);
        out.println(DELIMITER);

        out.println(isYearLeap(400));
        out.println(isYearLeap(100));
        out.println(isYearLeap(2016));
    }

    public static void printThreeWords() {
        String[] strings = {"Orange", "Banana", "Apple"};
        for (String str : strings) {
            out.println(str);
        }
    }

    public static void checkSumSign(int a, int b) {
        out.println(a + b >= 0 ? "The amount is positive" : "The amount is negative");
    }

    public static void printColor(int value) {
        if (value <= 0) {
            out.println("Red");
        } else if (value <= 100) {
            out.println("Yellow");
        } else {
            out.println("Green");
        }
    }

    public static void compareNumbers(int a, int b) {
        out.println(a >= b ? "a >= b" : "a < b");
    }

    public static boolean compareSumOfTwoNumbers(int a, int b) {
        int sum = a + b;
        return sum >= 10 && sum <= 20;
    }

    public static void numberIsPositiveOrNegative(int number) {
        out.println(number >= 0 ? "Number is positive" : "Number is negative");
    }

    public static boolean isNumberNegative(int number) {
        return number < 0;
    }

    public static void printStringFewTimes(String str, int printTimes) {
        if (str == null || printTimes <= 0) {
            throw new IllegalArgumentException("String is null or printTimes is negative!");
        }
        for (int i = 0; i < printTimes; i++) {
            out.println(str);
        }
    }

    public static boolean isYearLeap(int year) {
        if (year <= 0) {
            throw new IllegalArgumentException("Input value can not be negative or zero!");
        }
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }

}
