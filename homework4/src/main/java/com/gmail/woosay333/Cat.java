package com.gmail.woosay333;

import static java.lang.System.out;

public class Cat extends Animal {

    private static int counterOfCreatedClasses = 0;
    private int runDistance;

    public Cat(String name) {
        super(name);
        this.runDistance = 200;
        ++counterOfCreatedClasses;
    }

    public static int getCounterOfCreatedClasses() {
        return counterOfCreatedClasses;
    }

    @Override
    public void run(int obstacleLength) {
        if (runDistance == 0) {
            out.println("Cat " + getName() + " cannot run any more...");
        } else if (runDistance - obstacleLength >= 0) {
            runDistance -= obstacleLength;
            out.println("Cat " + getName() + " ran " + obstacleLength + " m.");
        } else {
            out.println("The length of the obstacle is too long...");
        }
    }

    @Override
    public void swim(int obstacleLength) {
        out.println("Sorry, cat " + getName() + " cannot swim...");
    }

    @Override
    public String toString() {
        return "Cat {" +
                "name = " + getName() +
                ", available to run: " + runDistance + " m." +
                '}';
    }

}
