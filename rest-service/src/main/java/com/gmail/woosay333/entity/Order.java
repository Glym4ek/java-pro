package com.gmail.woosay333.entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Order {

    private final UUID id = UUID.randomUUID();
    private final Date date = new Date();
    private final List<Product> productList = new ArrayList<>();
    private double cost;

    public Order() {
    }

    public void addProduct(Product product) {
        productList.add(product);
        cost += product.getCost();
    }

    public UUID getId() {
        return id;
    }

    public String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    public List<Product> getProductList() {
        return productList;
    }

    public double getCost() {
        return cost;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        return "Order{" +
                "id=" + id +
                ", date=" + simpleDateFormat.format(date) +
                ", productList=" + productList +
                ", cost=" + cost +
                '}';
    }

}
