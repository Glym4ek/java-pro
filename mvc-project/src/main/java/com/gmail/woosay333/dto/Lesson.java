package com.gmail.woosay333.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Lesson {

    private int id;
    private String name;
    private Homework homework;

}
