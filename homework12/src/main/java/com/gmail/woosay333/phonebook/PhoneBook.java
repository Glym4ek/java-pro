package com.gmail.woosay333.phonebook;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PhoneBook {

    private final List<Record> records;

    public PhoneBook() {
        this.records = new ArrayList<>();
    }

    public void add(Record note) {
        if (note == null) {
            throw new IllegalArgumentException("Record is null!");
        }

        records.add(note);
    }

    public Record find(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Input name is null!");
        }

        for (Record note : records) {
            if (note.getName().equals(name)) {
                return note;
            }
        }

        return null;
    }

    public List<Record> findAll(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Input name is null!");
        }

        List<Record> list = records.stream()
                .filter(note -> note.getName().equals(name))
                .collect(Collectors.toList());
        return list.isEmpty() ? null : list;
    }

    @Override
    public String toString() {
        return "PhoneBook {" +
                "records=" + records +
                '}';
    }

}
