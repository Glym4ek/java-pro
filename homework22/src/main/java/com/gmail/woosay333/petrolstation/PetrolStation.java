package com.gmail.woosay333.petrolstation;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Semaphore;

import static java.lang.Math.random;
import static java.lang.System.out;
import static java.lang.Thread.sleep;

public class PetrolStation {

    private volatile double amount;
    private final Semaphore semaphore;
    private final Map<UUID, Double> operations;

    public PetrolStation(double amount, int request) {
        this.amount = amount;
        this.semaphore = new Semaphore(request);
        this.operations = new HashMap<>();
    }

    public void doRefuel(double amount) {
        UUID id = UUID.randomUUID();

        if (!checkAmount(id, amount)) {
            out.println("Not enough fuel. Refueling is not possible. Available " + getAvailableAmount());
            return;
        }

        try {
            semaphore.acquire();

            out.println("Refueling has started...");
            sleep((long) (random() * 7000 + 3000));

            refuelOperation(id);

            out.println("Refueling completed, available " + getAvailableAmount() + " fuel");
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            ex.printStackTrace();
        } finally {
            semaphore.release();
        }
    }

    private synchronized boolean checkAmount(UUID id, double amount) {
        if (getAvailableAmount() - amount >= 0) {
            operations.put(id, amount);
            return true;
        }

        return false;
    }

    private synchronized double getAvailableAmount() {
        return amount - operations.values().stream().reduce(0.0, Double::sum);
    }

    private synchronized void refuelOperation(UUID id) {
        amount -= operations.get(id);
        operations.remove(id);
    }

}
