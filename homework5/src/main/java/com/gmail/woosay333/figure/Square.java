package com.gmail.woosay333.figure;

import static java.lang.Math.pow;

public class Square implements Figure {

    private final double side;

    public Square(double side) {
        if (side < 0) {
            throw new IllegalArgumentException("Square is not valid...");
        }
        this.side = side;
    }

    @Override
    public double getArea() {
        return pow(side, 2);
    }

    @Override
    public String toString() {
        return "Square {" +
                "side = " + side +
                '}';
    }

}
