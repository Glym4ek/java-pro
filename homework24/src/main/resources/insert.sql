-- homework.Homework insert
INSERT INTO homework.Homework
    (name, description)
VALUES ('GIT homework', 'basic git commands'),
       ('Introduction to OOP homework',
        'Create a class Employee, Create two classes with the same name SameName'),
       ('Inheritance in Java homework', 'Create Dog and Cat classes inheriting from Animal class'),
       ('Polymorphism in Java homework',
        'Create a basic interface for a geometric shape that has a method that returns the area of the shape. Create 3 classes that implement this interface: circle, triangle, square'),
       ('Primitive and reference data types homework',
        'create a HomeWorkApp class with methods printThreeWords(), checkSumSign(), printColor()'),
       ('Symbols and strings homework',
        'Create methods findSymbolOccurance(), findWordPosition(), stringReverse()'),
       ('Exceptions homework', 'Create ArrayValueCalculator class with doCalc() method'),
       ('Work with files homework',
        'Create a FileLogger class. The class will log (protocol) information to the specified file based on the configuration object'),
       ('Unit testing homework',
        'Write a method to which a non-empty one-dimensional integral array is passed as an argument. The method should return a new array obtained by extracting from the original array the elements that come after the last four'),
       ('Collections homework',
        'Create a countOccurance method that takes as input a string list as a parameter and an arbitrary string');


-- homework.Lesson insert
INSERT INTO homework.Lesson
    (name, updatedAt, homework_id)
VALUES ('GIT', '2023-01-27 12:00:00', 1),
       ('Introduction to OOP', '2023-02-07 22:14:00', 2),
       ('Inheritance in Java', '2023-02-08 16:45:00', 3),
       ('Polymorphism in Java', '2023-02-14 22:11:00', 4),
       ('Primitive and reference data types', '2023-02-20 09:23:00', 5),
       ('Symbols and strings', '2023-02-24 19:13:00', 6),
       ('Exceptions', '2023-02-25 14:37:00', 7),
       ('Work with files', '2023-03-02 22:23:00', 8),
       ('Unit testing', '2023-03-21 16:36:00', 9),
       ('Collections', '2023-03-22 17:12:00', 10);


-- homework.Schedule insert
INSERT INTO homework.Schedule
    (name, updatedAt)
VALUES ('Lesson 1', '2023-01-27 19:15:00'),
       ('Lesson 2', '2023-02-07 19:15:00'),
       ('Lesson 3', '2023-02-08 19:15:00'),
       ('Lesson 4', '2023-02-14 19:15:00'),
       ('Lesson 5', '2023-02-20 19:15:00'),
       ('Lesson 6', '2023-02-24 19:15:00'),
       ('Lesson 7', '2023-02-25 19:15:00'),
       ('Lesson 8', '2023-03-02 19:15:00'),
       ('Lesson 9', '2023-01-27 19:15:00'),
       ('Lesson 10', '2023-03-22 19:15:00');


-- homework.Lesson_Schedule insert
INSERT INTO homework.Lesson_Schedule
    (lesson_id, schedule_id)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5),
       (6, 6),
       (7, 7),
       (8, 8),
       (9, 9),
       (10, 10);
